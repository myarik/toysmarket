from django.conf.urls import patterns, include, url

from toysmarket.apps.product.views import UploadCsvFile
from django.contrib import admin

from django.views.generic import TemplateView

from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('toysmarket.apps.product.urls')),
    url(r'^upload_csv/', UploadCsvFile.as_view(), name='upload_csv'),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^error_message/',
        TemplateView.as_view(template_name="error_page.html"),
        name='error_message'),
    url(r'^accounts/', include('toysmarket.apps.accounts.urls')),
    url(r'^order/', include('toysmarket.apps.order.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^robots.txt$',
        TemplateView.as_view(template_name="robots.txt"), name='robots.txt'),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(
            r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}
        ),
    )
