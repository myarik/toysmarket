# -*- makefile -*-

# definitions
PROJECT       = toysmarket
HOST          = 127.0.0.1
PORT          = 8895
CURRPATH      = $(shell pwd)
PIDFILE       = $(shell pwd)/etc/django.pid

SPHINXPATH    = /opt/sphinx

MANAGE=cd $(PROJECT) && PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=$(PROJECT).settings django-admin.py

clean:
	@echo Cleaning up *.pyc files
	-find . | grep '.pyc$$' | xargs -I {} rm {}

run:
	$(MAKE) clean
	$(MANAGE) runserver_plus $(HOST):$(PORT)

shell:
	$(MAKE) clean
	$(MANAGE) shell_plus

dbshell:
	$(MAKE) clean
	$(MANAGE) dbshell

clean:
	@echo Cleaning up *.pyc files
	-find . | grep '.pyc$$' | xargs -I {} rm {}

uwsgi_reload:
	touch /tmp/toysmarket.txt

sphinx_start:
	sudo supervisorctl restart sphinx

sphinx_stop:
	sudo supervisorctl stop sphinx

sphinx_restart:
	sudo supervisorctl restart sphinx

sphinx_local:
# a shortcut to restart sphinx if no supervisor available
	-$(SPHINXPATH)/bin/searchd --stopwait
	$(SPHINXPATH)/bin/searchd -c etc/sphinx.conf

sphinx_reindex:
	$(SPHINXPATH)/bin/indexer -c etc/sphinx.conf --rotate --all

sphinx_config:
	$(MANAGE) sphinx_config

celery_run:
	$(MANAGE) celery worker -E --loglevel=info

celery_view:
	$(MANAGE) celery events

celery_for_admin:
	$(MANAGE) celerycam -F 60 --loglevel=info
