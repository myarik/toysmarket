/************************************/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function sameOrigin(url) {
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});
/************************************/

function priceSearch(){
    var searchBlock = $(this).closest('.filters-bottom');
    var catalog_id = searchBlock.data('catalog_id');
    var min_price = +searchBlock.find(".text-field").val();
    var max_price = +searchBlock.find(".text-field2").val();
    var search_url = '/search/?min_price='+min_price+'&max_price='+max_price+'&catalog_id='+catalog_id;
    window.location.href=search_url;
    return false;
}

function addToCart(itemId){
    var itemBlock =  $(this).closest('.goods');
    var itemAmount = +itemBlock.find('.number-field').val()
    if (itemAmount == 0) {
        itemAmount = 1;
        itemBlock.find('.number-field').val(1)
    };
    itemBlock.addClass('on_basket');
    addProduct(itemId, itemAmount);
    return false;
}

function addProduct(itemId, itemAmount) {
    value = {
        'product': itemId,
        'quantity': itemAmount
    }
    $.ajax({
        url: '/order/add/',
        type: 'POST',
        dataType: 'json',
        data: value,
    })
        .done(function(data) {
            if (data.success == true) {
                notif({
                    type: "success",
                    msg: "Товар добавлен",
                    position: "right",
                    width: 200,
                    height: 40,
                    timeout: 2000,
                    fade: true
                });
                $('.basket-goods').text(data.count + ' товара ' + data.data.price);
                if ($(".basket-all-sum-number")){
                    $(".basket-all-sum-number").text(data.data.price + " грн");
                }
            } else {
                notif({
                  msg: data.error.message,
                  type: "error",
                  width: 550,
                  timeout: 3000,
                });
            }
        })
        .fail(function(xhr, textStatus) {
            if (xhr.readyState == 4) {
            } else {
                notif({
                  msg: "Внутренняя ошибка сервера",
                  type: "error",
                  width: 500,
                  timeout: 3000,
                });
            }
        });
    return false;
}

function RemoveGoodsBasket(id) {
    var itemBlock = $(this).closest('tr');
    $.ajax({
        url: '/order/product_del/',
        type: 'POST',
        dataType: 'json',
        data: {
            product: id,
        },
    })
        .done(function(data) {
            if (data.success == true) {
                notif({
                    type: "warning",
                    msg: "Товар Удале",
                    position: "right",
                    width: 200,
                    height: 40,
                    timeout: 2000,
                    fade: true
                });
                itemBlock.hide();
                if (data.data) {
                    $('.basket-goods').text(data.count + ' товара ' + data.data.price);
                    $('.basket-all-sum-number').text(data.data.price);
                }else{
                    $('.basket-goods').text('нет товара');
                    $('.basket-all-sum-number').text('0');
                    $('.goods-button').hide();
                    $('.basket-form').hide();
                }

            } else {
                notif({
                  msg: data.error.message,
                  type: "error",
                  width: 550,
                  timeout: 3000,
                });
            }
        })
        .fail(function() {
            notif({
                  msg: "Внутренняя ошибка сервера",
                  type: "error",
                  width: 500,
                  timeout: 3000,
            });
        });
    return false;
}

function SetUserAddress(city) {
    var result = true;
    $.ajax({
        url: '/accounts/user_address/',
        type: 'POST',
        data: {
            city: city,
        },
        async: false
    })
        .done(function(data, xhr, textStatus) {
            if (data.success != true) {
                result = false;
            }
        })
        .fail(function(xhr, textStatus) {
            result = false;
        });
        return result
}

function PayOrder(delivery, payment, comment){
    result = false;
    $.ajax({
        url: '/order/approve/',
        type: 'GET',
        dataType: 'json',
        data: {
            delivery: delivery,
            payment_type: payment,
            comment: comment
        },
        async: false
    })
        .done(function(data) {
            if (data.success == true) {
                result = true;
            } else {
                notif({
                  msg: data.error.message,
                  type: "error",
                  width: 550,
                  timeout: 3000,
                });
            }
        })
        .fail(function(xhr, textStatus) {
            notif({
                  msg: "Внутренняя ошибка сервера",
                  type: "error",
                  width: 500,
                  timeout: 3000,
            });
        });
    return result;
}

function approveOrder() {
    var basketForm = $(this).closest('.basket-form');
    var city = basketForm.find('input[name=city]').val();
    basketForm.find('.control-group.error').each(function() {
        $(this).hide();
    });
    if(city == ""){
        basketForm.find('.basket-form-button').before('<div class="control-group error">Введите город доставки<div>');
        return false;
    }
    SetUserAddress(city);
    var delivery = basketForm.find('#delivery').val();
    var payment = basketForm.find('#payment').val();
    var comment = basketForm.find('#comment').val();
    if (PayOrder(delivery, payment, comment)) {
       window.location='/accounts/profile';
    }
    return false;
}

$(document).ready(function(){
    $('.filters-bottom').on('click', 'button', priceSearch);
    $('input[type=number]').on('click', function () {
        var amount = +$(this).val()
        if (amount < 0) {
            $(this).val('1');
        };
    });
    $('input[type=number]').on('mouseleave', function () {
        var amount = +$(this).val()
        if (amount < 0) {
            $(this).val('1');
        };
    });
    $('.basket-form-button').on('click', 'button', approveOrder);
    $('.basket-field-amt').on('keyup', function(){
        var amount = +$(this).val()
        if (amount < 0) {
            $(this).val('1');
            amount = 1;
        };
        if (amount > 0){
            var str = $(this).closest('tr').find('.basket-tb-cell-price').html();
            var price = parseFloat(str.substring(0, str.length - 3).replace(",", "."));
            var sum = (amount * price) + "грн";
            $(this).closest('tr').find('.basket-tb-cell-sum').text(sum);
            addProduct($(this).attr('id'), amount);
        }
    });
    $('.basket-field-amt').on('click', function(){
        var amount = +$(this).val()
        if (amount < 0) {
            $(this).val('1');
            amount = 1;
        };
        if (amount > 0){
            var str = $(this).closest('tr').find('.basket-tb-cell-price').html();
            var price = parseFloat(str.substring(0, str.length - 3).replace(",", "."));
            var sum = (amount * price) + "грн";
            $(this).closest('tr').find('.basket-tb-cell-sum').text(sum);
            addProduct($(this).attr('id'), amount);
        }
    });
    $('.img_fancybox').fancybox({
        padding: 0,
        openEffect  : 'elastic',
        closeEffect : 'elastic',
        closeClick: true,
        closeBtn: true,
        helpers: {
            overlay: {
                locked: false
            },
            title : {
                type : 'inside'
            }
        }
    });
    $('.basket-footer').on('click', '.button', function(event) {
        event.preventDefault();
        $('.basket-form').toggle();
    });
    $('.tabs-js').tabs();
    $('.bxslider').bxSlider({
        slideWidth: 200,
        maxSlides: 3,
        slideMargin: 10,
        moveSlides: 1,
        auto: true
    });
    if ( $('#messagebox').length > 0 ){
        notif({
            msg: $('#messagebox').data('msg'),
            type: $('#messagebox').data('notify'),
            timeout: 3000,
        });
    }
}); // Ready
// tabs
(function($){
tabs = function() {
    this._init = function(element, options) {
        var defaults = {
            this: $(element),
            tabCurrent : 'tab-current',
            tabSection : '.tabs-section-js',
            tabContent : '.tab-content-js',
            inDelay : 150
        },
        settings = $.extend(defaults, options);

        settings.this.delegate('li:not(.'+settings.tabCurrent+')', 'click', function() {
            var $el = $(this);
            $el
                .addClass(settings.tabCurrent)
                .siblings().removeClass(settings.tabCurrent)
                .parents(settings.tabSection).find(settings.tabContent).eq($el.index()).fadeIn(settings.inDelay)
                .siblings(settings.tabContent).hide();
        });
    };
};
    // Launch plugin
    $.fn.tabs = function(options){
        return this.each(function(){
             $(this).data( "tabs", new tabs()._init( this, options ) );
         });
    };
})($);
// end tabs
