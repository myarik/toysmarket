# -*- coding: utf-8 -*-
from .utils import *

import djcelery
import sys

djcelery.setup_loader()
DEBUG = False
TEMPLATE_DEBUG = DEBUG


sys.path.append(root('apps'))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'toys',
        'USER': 'toys',
        'PASSWORD': 'waihae9O',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {'charset': 'utf8',
                    "init_command": "SET storage_engine=INNODB, \
                                     SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED"},
        'TEST_NAME': 'toys_test',
        'TEST_CHARSET': 'utf8',
        'TEST_COLLATION': 'utf8_general_ci'
    }
}


ALLOWED_HOSTS = ['162.243.62.147', '127.0.0.1']

LANGUAGE_CODE = 'ru-ru'  # 'en-us'

TIME_ZONE = 'Europe/Kiev'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

CACHES = {
    'default': {
        'BACKEND': 'toysmarket.apps.core.cache.RedisCache',
        'LOCATION': 'localhost:6379',
        'OPTIONS': {
            'DB': 1,
            'PARSER_CLASS': 'redis.connection.HiredisParser'
        },
    }
}

# Redis booking storage backend (host, port, db, password)
REDIS_STORAGE = ('localhost', 6379, 0, None)

BASKET_SESSION_TIMEOUT = 60 * 60 * 2  # 2 hour

SESSION_ENGINE = 'order.core.session_backend'
# dj_skeleton/dj_skeleton/media
MEDIA_ROOT = root('media')

MEDIA_URL = '/media/'

# dj_skeleton/dj_skeleton/static
STATIC_ROOT = root('static')

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    root('static/common'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'c!e9765iwpdusq#6mnx$=6)jh9%_)&hee55_k__7=hg@n%vrj5'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'order.context_processors.basket_info',
    'extra_settings.context_processors.phone_info',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#Cache
CACHE_MIDDLEWARE_KEY_PREFIX = 'toys'

ROOT_URLCONF = 'urls'

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

TEMPLATE_DIRS = (
    root('templates'),
)

#Sphinx
SPHINX_PATH = "/opt/sphinx"  # for config generation
SPHINX_HOST = '127.0.0.1'
SPHINX_PORT = 9306
SPHINX_INDEX = "product"
SPHINX_MAX_MATCHES = 1000

#Celery
BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_DISABLE_RATE_LIMITS = True
CELERY_TASK_RESULT_EXPIRES = 43200
CELERY_SEND_TASK_ERROR_EMAILS = True
CELERY_DEFAULT_QUEUE = 'celery'


# Application definition
INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',  # <----- here!
    'django.contrib.flatpages',

    'south',
    'django_jenkins',
    'bootstrap3',
    'simple_email_confirmation',
    'ckeditor',
    'sortable_listview',
    'emails',
    'yacaptcha',

    'product',
    'accounts',
    'order',
    'extra_settings',
)

AUTH_USER_MODEL = 'accounts.User'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'extended': {
            'format': '%(levelname)s %(message)s %(args)s',
        },
        'standard': {
            'format': u'[%(asctime)s %(levelname)s] %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file.upload_csv': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 3,
            'formatter': 'extended',
            'filename': root('../logs/upload_csv.log'),
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'upload_csv': {
            'handlers': ['file.upload_csv'],
            'level': 'INFO'
        },
    },

}
#Grapelly
GRAPPELLI_ADMIN_TITLE = 'Toys Market'
GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'

#IMAGES_DIR
IMAGES_DIR = root('../upload_new_images')

#CKEDITOR
# CKEditor upload path change
CKEDITOR_UPLOAD_PATH = root("media/uploads")

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Full',
        'height': 300,
        'width': 500,
        'language': 'ru',
    },
}
CKEDITOR_IMAGE_BACKEND = 'pillow'

# default mail
EMAIL_FROM_ADDR = 'notify@777toys.com.ua'
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True

YANDEX_KEY = ''

try:
    from local import *
except ImportError:
    print 'please add local.py'

try:
    from test import *
except ImportError:
    print 'please add test.py'
