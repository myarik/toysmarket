# -*- coding: utf-8 -*-
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

root = lambda *x: os.path.abspath(os.path.join(os.path.abspath(BASE_DIR), *x))
