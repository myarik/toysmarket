# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext as _

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils import timezone

from django.contrib.sites.models import Site

from imagekit.models import ProcessedImageField
from pilkit.processors.resize import ResizeToFill
from simple_email_confirmation import SimpleEmailConfirmationUserMixin

from celery import current_app



class MyUserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        email = MyUserManager.normalize_email(email)
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=email, is_staff=False, is_active=True, is_superuser=False,
                          last_login=now, created_at=now, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        u = self.create_user(email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)
        return u


class Address(models.Model):
    """
        Adress model
    """
    city = models.CharField(max_length=255, verbose_name=_('Город'))
    address = models.CharField(max_length=255, verbose_name=_('Адрес'),
                               blank=True, null=True)
    postcode = models.CharField(max_length=255, verbose_name=_('Индекс'),
                                blank=True, null=True)
    user = models.ForeignKey('User', verbose_name=_('Пользователь'),
                             related_name='adress')

    def __unicode__(self):
        return unicode(self.user)

    class Meta:
        verbose_name = _('Адрес')
        verbose_name_plural = _('Адреса')


class User(SimpleEmailConfirmationUserMixin, AbstractBaseUser, PermissionsMixin):
    GENDER = (
        (0, _('Не указан')),
        (1, _('Мужчина')),
        (2, _('Женщина')),
    )

    username = models.CharField(
        max_length=100, verbose_name=_('Имя пользователя'), blank=True, null=True)
    email = models.EmailField(unique=True, db_index=True,
                              verbose_name=('E-mail'))
    avatar = ProcessedImageField(upload_to='avatars',
                                 processors=[ResizeToFill(100, 50)],
                                 format='JPEG',
                                 options={'quality': 95},
                                 blank=True, null=True)
    phone = models.CharField(
        max_length=255, verbose_name=_('Телефон'), blank=True, null=True)
    ip_adress = models.CharField(max_length=255, verbose_name=_('IP-адрес'),
                                 blank=True, null=True)
    gender = models.IntegerField(choices=GENDER, default=0,
                                 verbose_name=_('Пол'))
    first_name = models.CharField(_('Имя'), max_length=30, blank=True)
    last_name = models.CharField(_('Фамилия'), max_length=30, blank=True)
    created_at = models.DateTimeField(_('Дата регистрации'), auto_now=True)

    is_active = models.BooleanField(default=True,
                                    verbose_name=_('Активный'))
    is_staff = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    def __unicode__(self):
        return unicode(self.email)

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        """
            Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def save(self, *args, **kwargs):
        update_model = bool(self.pk)
        super(User, self).save(*args, **kwargs)
        if not update_model:
            user_confirm = {
                'key': self.confirmation_key,
                'uid': self.id,
                'name': self.first_name,
                'last_name': self.last_name,
                'SITE_URL': Site.objects.get_current().domain
            }
            current_app.send_task(
                'emails.send_email',
                args=[
                    'registration',
                    self.email,
                    user_confirm,
                    u'777toys.com.ua - Регистрация на сайте'
                ],
            )

