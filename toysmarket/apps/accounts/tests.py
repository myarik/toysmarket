# -*- coding: utf-8 -*-

from django_webtest import WebTest
from model_mommy import mommy
from toysmarket.apps.accounts.forms import UserCreationForm, UserChangeForm

from toysmarket.apps.accounts.models import Address, User

from django.core.urlresolvers import reverse


class ModelTest(WebTest):
    """
    Test Models
    """
    def setUp(self):
        self.user = mommy.make(
            User,
            first_name = 'Vasya',
            last_name = 'Pupkin',
            email='example@ex.com'
        )
        self.address = mommy.make(
            Address,
            user = self.user
        )

    def test_model_user(self):
        self.assertTrue(isinstance(self.user, User))
        self.assertFalse(self.user.is_confirmed)
        self.assertTrue(self.user.is_active)

    def test_model_address(self):
        self.assertTrue(isinstance(self.address, Address))


class FormsTest(WebTest):
    """
    Test Form
    """
    def test_valid_form(self):
        data = {
            'first_name': 'Vasya',
            'last_name': 'Pupkin',
            'email': 'email@ex.com',
            'password1': 'test123',
            'gender': '1',
            'password2': 'test123'
        }
        form = UserCreationForm(data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'first_name': 'Vasya',
            'last_name': 'Pupkin',
            'email': 'email@ex.com',
            'password1': 'test123',
            'password2': 'test1234'
        }
        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())

    def test_valid_form_change(self):
        data = {
            'first_name': 'Vasya',
            'last_name': 'Pupkin',
            'email': 'email@ex.com',
            'phone': '0442586542',
            'gender': '1'
        }
        form = UserChangeForm(data)
        self.assertTrue(form.is_valid())


class ViewTest(WebTest):
    """
    Test View
    """
    csrf_checks = False

    def setUp(self):
        self.user = mommy.make(
            User,
            first_name = 'Vasya',
            last_name = 'Pupkin',
            email='example@ex.com',
        )
        self.user.set_password('pass')
        self.user.save()

    def test_registration(self):
        resp = self.app.get(reverse('registration'))
        self.assertEqual(resp.status_code, 200)
        form = resp.forms['registration']
        form['first_name'] = 'Vasya'
        form['last_name'] = 'Pupkin'
        form['email'] = 'e@mail.ru'
        form['gender'] = '1'
        form['password1'] = 'test123'
        form['password2'] = 'test123'
        form.submit()
        self.assertEqual(User.objects.all().count(), 2)

    def test_user_edit(self):
        resp = self.app.get(reverse('user_edit'), user=self.user)
        self.assertEqual(resp.status_code, 200)
        form = resp.forms['user_edit']
        form['first_name'] = 'Alex'
        form['last_name'] = 'Pupkin'
        form['email'] = 'e@mail.ru'
        form['gender'] = '1'
        form['phone'] = '0448542645'
        form.submit()
        self.assertEqual(
            User.objects.get(pk=self.user.pk).email,
            'e@mail.ru'
        )

    def test_confirm_email(self):
        resp = self.app.get(reverse('confirm_email', kwargs={'key': 'sddsa', 'uid': self.user.id}))
        self.assertFalse(User.objects.get(pk=self.user.pk).is_confirmed)
        resp = self.app.get(reverse('confirm_email', kwargs={'key': self.user.confirmation_key, 'uid': self.user.id}))
        self.assertTrue(User.objects.get(pk=self.user.pk).is_confirmed)
