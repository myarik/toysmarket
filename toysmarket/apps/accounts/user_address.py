# -*- coding: utf-8 -*-
from toysmarket.apps.core.base_struct import BaseStruct
from toysmarket.apps.core.error_exeption import ValidateError
from toysmarket.apps.accounts.models import Address


class UserAddress(BaseStruct):
    """User address save to database"""
    user = None
    city = None
    _list_fld = (
        'city',
    )

    def validate(self):
        """
            Check all value
        """
        if not self.user:
            raise ValidateError(message=u"Пользователь не найден")
        if not self.city:
            raise ValidateError(message=u"Укажите город доставки")

    def check_diff(self, user_address):
        for item in self.get_field_list():
            if item in self._list_fld:
                if getattr(user_address, item) != getattr(self, item):
                    return False
        return True

    def process(self):
        _address = Address.objects.filter(user=self.user)
        if _address:
            user_address = _address[0]
            if not self.check_diff(user_address):
                for fld in self._list_fld:
                    setattr(user_address, fld, getattr(self, fld))
                user_address.save()
        else:
            self.user.adress.create(
                city=self.city,
            )
