# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.shortcuts import redirect

from django.views.generic.base import View, TemplateView
from django.views.generic.edit import CreateView, FormView

from toysmarket.apps.accounts.forms import (UserCreationForm, UserChangeForm,
                                            MyAuthenticationForm)
from toysmarket.apps.accounts.models import User

from django.contrib import messages

from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache

from django.conf import settings
from django.contrib.sites.models import Site

from django.contrib.auth import login as auth_login, logout as auth_logout

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from toysmarket.apps.core.utils import ajax_request
from toysmarket.apps.accounts.user_address import UserAddress

from toysmarket.apps.core.error_exeption import ValidateError

from toysmarket.apps.order.core.storage import RedisOrderStorage
from toysmarket.apps.order.models import Delivery, Order

from celery import current_app


class CreateUserView(CreateView):
    """regisration user"""
    template_name = "accounts/regisration.html"
    form_class = UserCreationForm
    success_url = '/'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.ip_adress = self.request.META['REMOTE_ADDR']
        messages.success(self.request, _("Спасибо за регистрации, на Ваш email отправленно письмо"))
        return super(CreateUserView, self).form_valid(form)

    def form_invalid(self, form):
        return super(CreateUserView, self).form_invalid(form)


class LoginUserView(FormView):
    form_class = MyAuthenticationForm
    template_name = "accounts/login.html"

    @method_decorator(never_cache)
    @method_decorator(sensitive_post_parameters('password'))
    def dispatch(self, request, *args, **kwargs):
        request.session.set_test_cookie()
        return super(LoginUserView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        redirect_to = settings.LOGIN_REDIRECT_URL
        auth_login(self.request, form.get_user())
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return redirect(redirect_to)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class Logout(View):
    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return redirect(settings.LOGOUT_REDIRECT_URL)


class UserEdit(FormView):
    form_class = UserChangeForm
    template_name = 'accounts/user_edit.html'
    success_url = '/accounts/profile/'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserEdit, self).dispatch(*args, **kwargs)

    def get_initial(self):
        user = self.request.user
        initial = {
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'username': user.username,
            'phone': user.phone,
            'gender': user.gender,
        }
        return initial

    def post(self, request, *args, **kwargs):
        form = UserChangeForm(request.POST,
                              request.FILES,
                              instance=request.user)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        old_data = User.objects.get(pk=self.request.user.pk)
        user = self.request.user
        for field, value in form.cleaned_data.iteritems():
            if field == 'email' and old_data.email != value:
                if not value in user.unconfirmed_emails \
                        and value not in user.confirmed_emails:
                    confirmation_key = user.add_unconfirmed_email(value)
                else:
                    confirmation_key = user.confirmation_key
                user_confirm = {
                    'key': confirmation_key,
                    'name': user.first_name,
                    'last_name': user.last_name,
                    'uid': user.id,
                    'SITE_URL': Site.objects.get_current().domain
                }
                current_app.send_task(
                    'emails.send_email',
                    args=[
                        'user_confirm',
                        value,
                        user_confirm,
                        u'777toys.com.ua - Изменения контактной информации'
                    ],
                )
            if value:
                setattr(user, field, value)
        user.save()
        return super(UserEdit, self).form_valid(form)


class ConfirmEmail(View):
    def get(self, request, key, uid):
        """ Email confirmation method """
        user = User.objects.get(id=uid)
        if user.is_confirmed:
            return redirect(reverse('index'))
        else:
            try:
                user.confirm_email(key)
                user.set_primary_email(user.email)
            except:
                messages.error(request, _('Не правильно указан ключ'))
                return redirect(reverse('error_message'))
            messages.success(request, _('Ваш ящик подтвержден успешно прошел проверку'))
            return redirect(reverse('index'))


class AccountInfoView(TemplateView):
    template_name = "accounts/user_view.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AccountInfoView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountInfoView, self).get_context_data(**kwargs)
        context['user'] = self.request.user
        context['address'] = \
            self.request.user.adress.all()[0] if self.request.user.adress.all() else None
        context['order_list'] = \
            self.request.user.userorder.all().order_by('-created_date')[:10]
        order = RedisOrderStorage(self.request).get_from_storage()
        if order:
            context['product_list'] = []
            for item in order.order_items:
                product = item.product
                product.count = item.quantity
                product.sum = item.quantity * product.price
                context['product_list'].append(product)
        context['delivery_list'] = Delivery.objects.all()
        context['payment_list'] = Order.PAYMENT_TYPE
        return context


@ajax_request()
def user_address(request):
    '''
    Save User Address request
    '''
    user_address = UserAddress.load_from_dict(request.POST)
    user_address.user = request.user
    try:
        user_address.validate()
    except ValidateError as err:
        return {
            'success': False,
            'error': {
                'message': err.message
            }
        }
    try:
        user_address.process()
    except:
        return {
            'success': False,
            'error': {
                'message': u'Произошла внутренняя ошибка сервера повторите запрос позже'
            }
        }
    return {'success': True}
