# -*- coding: utf-8 -*-
from django import template
from toysmarket.apps.accounts.models import User

register = template.Library()


@register.filter(name='user_city')
def user_city(user_id):
    if user_id:
        user = User.objects.get(pk=user_id)
        user_address = user.adress.all()
        if user_address:
            return user_address[0].city
    return ''
