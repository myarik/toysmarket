# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib.auth.forms import AdminPasswordChangeForm
from toysmarket.apps.accounts.views import (CreateUserView, LoginUserView,
                                            AccountInfoView, Logout, UserEdit,
                                            ConfirmEmail)

from django.contrib.auth import views as auth_views


urlpatterns = patterns('',
                       url(r'^password/change/$', auth_views.password_change,
                           {'template_name': 'accounts/password_change.html', }, name='password_change'),
                       url(
                           r'^password/change/done/$',
                           auth_views.password_change_done,
                           {'template_name': 'accounts/password_change_done.html'},
                           name='password_change_done'),
                       url(
                           r'^password/reset/$',
                           auth_views.password_reset,
                           {'template_name': 'accounts/password_reset.html'},
                           name='password_reset'),
                       url(r'^password/reset/done/$',
                           auth_views.password_reset_done,
                           {'template_name': 'accounts/password_reset_done.html'},
                           name='password_reset_done'),
                       url(
                           r'^password/reset/complete/$',
                           auth_views.password_reset_complete,
                           {'template_name': 'accounts/password_reset_complete.html'},
                           name='password_reset_complete'),
                       url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           auth_views.password_reset_confirm,
                           {'template_name': 'accounts/password_reset_confirm.html'},
                           name='password_reset_confirm'),
                       url(r'^registration/$', CreateUserView.as_view(), name='registration'),
                       url(r'^login/$', LoginUserView.as_view(), name='login'),
                       url(r'^logout/$', Logout.as_view(), name='logout'),
                       url(r'^edit/$', UserEdit.as_view(), name='user_edit'),
                       url(r'^confirm/(?P<key>[^/]+)/(?P<uid>[0-9]+)/$',
                           ConfirmEmail.as_view(), name='confirm_email'),
                       url(r'^user_address/$',
                           'accounts.views.user_address', name='user_address'),
                       url(r'^profile/$', AccountInfoView.as_view(),
                           name='profile')
)
