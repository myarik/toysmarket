# -*- coding: utf-8 -*-
from django import forms

from django.utils.translation import ugettext_lazy as _
from toysmarket.apps.accounts.models import User
from yacaptcha.fields import YandexCaptchField

from django.contrib.auth.forms import AuthenticationForm

import re


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Подтверждение пароля', widget=forms.PasswordInput)
    captcha = YandexCaptchField()

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            msg = u"Пароли не совпадают"
            raise forms.ValidationError(msg)
        return password2

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = u'E-mail'
        self.fields['email'].widget.attrs['class'] = u'simple-input'
        self.fields['password1'].widget.attrs['placeholder'] = u'Пароль'
        self.fields['password1'].widget.attrs['class'] = u'simple-input'
        self.fields['password2'].widget.attrs['placeholder'] = \
            u'Подтверждение пароля'
        self.fields['password2'].widget.attrs['class'] = u'simple-input'
        self.fields['first_name'].widget.attrs['placeholder'] = u'Имя'
        self.fields['first_name'].widget.attrs['class'] = u'simple-input'
        self.fields['last_name'].widget.attrs['placeholder'] = u'Фамилия'
        self.fields['last_name'].widget.attrs['class'] = u'simple-input'
        self.fields['gender'].widget.attrs['class'] = u'simple-select'
        self.fields['last_name'].widget.attrs['class'] = u'simple-input'

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = u'E-mail'
        self.fields['email'].widget.attrs['class'] = u'simple-input'
        self.fields['first_name'].widget.attrs['placeholder'] = u'Имя'
        self.fields['first_name'].widget.attrs['class'] = u'simple-input'
        self.fields['last_name'].widget.attrs['placeholder'] = u'Фамилия'
        self.fields['last_name'].widget.attrs['class'] = u'simple-input'
        self.fields['gender'].widget.attrs['class'] = u'simple-select'
        self.fields['phone'].widget.attrs['class'] = u'simple-input'
        self.fields['phone'].widget.attrs['placeholder'] = u'Телефон'

    def clean_email(self):
        if User.objects.filter(email__iexact=self.cleaned_data['email']).exclude(id=self.instance.id):
            raise forms.ValidationError(
                u'Пользователь с таким почтовым ящиком уже существует')
        return self.cleaned_data['email']

    def clean_phone(self):
        if self.cleaned_data['phone']:
            phone_digits = re.compile(r'^\d\d\d\s?\d\d\d\s?\d\d\s?\d\d$')
            if not phone_digits.match(str(self.cleaned_data['phone'])):
                raise forms.ValidationError(
                    u'Номер телефона нужно вводить в формате XXX XXX XX XX.')
        return self.cleaned_data['phone']

    class Meta:
        model = User
        fields = ('email', 'phone', 'gender', 'first_name', 'last_name')


class MyAuthenticationForm(AuthenticationForm):

    def __init__(self, request=None, *args, **kwargs):
        super(MyAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = u'E-mail'
        self.fields['username'].widget.attrs['class'] = u'simple-input'
        self.fields['password'].widget.attrs['placeholder'] = u'Password'
        self.fields['password'].widget.attrs['class'] = u'simple-input'
