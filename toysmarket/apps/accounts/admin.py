# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.

from django.utils.translation import ugettext as _
from django.contrib.auth.admin import UserAdmin
from forms import UserCreationForm, UserChangeForm
from models import User, Address


class UserAdmin(UserAdmin):
    """ User admin page """
    add_form = UserCreationForm
    form = UserChangeForm

    list_display = ('email', 'phone', 'is_staff', 'is_confirmed')
    list_filter = ('is_staff', 'is_active',)
    search_fields = ('email', 'phone',)
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)
    fieldsets = (
        (None, {'fields': ('email', 'gender',
                           'phone', 'ip_adress',
                           'first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active',
                                       'is_staff',
                                       'is_superuser',)}),
        ('Important dates', {'fields': ('last_login',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'password1', 'password2')}),
    )


class AddressAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Address, AddressAdmin)

