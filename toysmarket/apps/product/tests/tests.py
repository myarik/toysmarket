# -*- coding: utf-8 -*-
import shutil
import logging

from django_webtest import WebTest
from model_mommy import mommy
from toysmarket.apps.product.forms import CsvFileForm

from django.core.files.uploadedfile import SimpleUploadedFile
from toysmarket.apps.product.models import Category, Product, CsvFile

from django.core.cache import cache
from django.conf import settings
from django.core.files import File

import datetime
import random
from toysmarket.apps.product.upload_product import UploadProduc

from toysmarket.settings.utils import root
from django.core.urlresolvers import reverse

from toysmarket.apps.core.error_exeption import ValidateError
from nose.tools import raises

from decimal import Decimal

from django.contrib.auth import get_user_model

import os


User = get_user_model()


class ModelTest(WebTest):

    def setUp(self):
        image = root('apps/product/tests/data_test/toys.jpg')
        self.category = mommy.make(Category, name=u"Boys", uid=1)
        self.product = mommy.make(
            Product,
            category=self.category,
            image_origin=File(open(image))
        )
        self.csv = mommy.make(CsvFile)

    def test_model_categoty(self):
        self.assertTrue(isinstance(self.category, Category))
        subcat = mommy.make(Category, name=u"Cars", parent=self.category)
        self.assertEqual(subcat.parent, self.category)
        self.assertEqual(subcat, self.category.children.all()[0])
        self.assertTrue(subcat.is_active)
        self.category.is_active = False
        self.category.save()
        self.assertFalse(Category.objects.get(pk=subcat.pk).is_active)
        self.product.delete()

    def test_model_product(self):
        self.assertTrue(isinstance(self.product, Product))
        self.assertTrue(self.product.category, self.category)
        self.product.delete()

    def test_model_csvfile(self):
        self.assertTrue(isinstance(self.csv, CsvFile))
        today = datetime.date.today()
        for i in xrange(15):
            upload_date = today - datetime.timedelta(days=random.randint(1, 20))
            item = mommy.make(CsvFile)
            item.upload_date = upload_date
            item.save()
        self.assertEqual(CsvFile.objects.all().count(), 10)
        self.product.delete()
        command='rm -rf %s/*' % root('media/csvfile')
        os.system(command)

    def test_manager(self):
        cat_disabled = mommy.make(Category, name=u"Girls", is_active=True)
        mommy.make(Product, category=cat_disabled)
        Category.objects.root_categories()
        self.assertEqual(Category.objects.active().count(), 2)
        self.assertEqual(len(Category.objects.root_categories()), 2)
        self.assertEqual(Product.objects.active().count(), 2)
        cat_disabled.is_active = False
        cat_disabled.save()
        self.assertEqual(Category.objects.active().count(), 1)
        self.assertEqual(Product.objects.active().count(), 1)
        self.product.delete()


class CatProdTest(WebTest):
    def setUp(self):
        self.Boys = mommy.make(Category, name=u"Boys", slug='')
        self.Cars = mommy.make(Category, name=u"Cars", slug='', parent=self.Boys)
        self.bmv = mommy.make(Product, category=self.Cars, name="CoolBmv", slug='')
        self.mrs = mommy.make(Product, category=self.Cars, name="Mersedes", slug='')
        self.lance_pierr = mommy.make(Product, category=self.Boys, name="lance-pierr", slug='')
        cache.clear()

    def test_cache_parent(self):
        cache_key = "%s-%s-category-parents" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.Cars.id)
        self.assertFalse(cache.get(cache_key))
        self.assertEqual(self.Cars.get_parents(), self.Boys)
        self.assertTrue(cache.get(cache_key))

    def test_cache_children(self):
        cache_key = "%s-%s-category-children" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.Boys.id)
        self.assertFalse(cache.get(cache_key))
        self.assertEqual(self.Boys.get_children()[0], self.Cars)
        self.assertTrue(cache.get(cache_key))

    def test_cache_get_products(self):
        cache_key = "%s-%s-category-products" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.Cars.id)
        self.assertFalse(cache.get(cache_key))
        self.assertEqual(len(self.Cars.get_products()), 2)
        self.assertTrue(cache.get(cache_key))
        key_2 = "%s-%s-category-products" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.Boys.id)
        self.assertFalse(cache.get(key_2))
        self.assertEqual(len(self.Boys.get_products()), 3)
        self.assertTrue(cache.get(key_2))
        mommy.make(Product, category=self.Cars, name="Fiat", slug='')
        self.assertFalse(cache.get(key_2))
        self.assertEqual(len(self.Boys.get_products()), 4)
        self.assertTrue(cache.get(key_2))
        self.Cars.is_active = False
        self.Cars.save()
        self.assertFalse(cache.get(key_2))
        self.assertEqual(len(self.Boys.get_products()), 1)


class UploadCsvTest(WebTest):
    # Upload file in test directory try test to upload
    logging.disable(logging.CRITICAL)
    fixtures = [root('../fixture/product_data.json')]

    def setUp(self):
        self.file_csv = root('apps/product/tests/data_test/obmen_site.csv')
        self.user = User.objects.create_superuser('Jim Carrey', 'jim@carrey.com')
        file_image = root('apps/product/tests/data_test/toys.jpg')
        if not os.path.isdir(settings.IMAGES_DIR):
            os.makedirs(settings.IMAGES_DIR)
        shutil.copy2(file_image, settings.IMAGES_DIR)
        self.product = mommy.make(
            Product,
            sku = u'13757',
            image_origin=File(open(file_image)),
            price=Decimal('19.69'),
            category=Category.objects.get(uid=0),
            total_sold=10
        )
        self.product_old = mommy.make(
            Product,
            sku = u'99999',
            image_origin=File(open(file_image)),
            price=Decimal('98.99'),
            category=Category.objects.get(uid=0),
            total_sold=10,
            items_in_stock=20,
            active=True
        )


    def test_valid_form(self):
        post_data = {
            'uploading_status': CsvFile.UPDATE_DISABLED,
            }
        upload_file = open(self.file_csv)
        file_data = {'csvfile': SimpleUploadedFile(upload_file.name, upload_file.read())}
        form = CsvFileForm(post_data, file_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'uploading_status': CsvFile.UPDATE_DISABLED,
            'csvfile': ''
        }
        form = CsvFileForm(data=data)
        self.assertFalse(form.is_valid())

    @raises(ValidateError)
    def test_create_class_wrong_status(self):
        err_upload = UploadProduc(
            csvfile_path='/apps/product/tests/data_test/obmen_site.csv',
            STATUS=8
        )

    def test_upload_class_reload(self):
        upload = UploadProduc(
            csvfile_path='/apps/product/tests/data_test/obmen_site.csv',
            STATUS=CsvFile.RELOAD
        )
        upload.process()
        self.assertFalse(Product.objects.filter(sku=self.product_old.sku))
        Product.objects.all().delete()

    def test_upload_class_remove(self):
        """
        Remove from site active=FALSE
        """
        upload = UploadProduc(
            csvfile_path='/apps/product/tests/data_test/obmen_site.csv',
            STATUS=CsvFile.UPDATE_REMOVE
        )
        upload.process()
        self.assertFalse(Product.objects.get(sku=self.product_old.sku).active)
        Product.objects.all().delete()

    def test_upload_class(self):
        upload = UploadProduc(
            csvfile_path='/apps/product/tests/data_test/obmen_site.csv',
            STATUS=CsvFile.UPDATE_DISABLED
        )
        upload.process()
        self.assertEqual(Product.objects.all().count(),19)
        self.assertEqual(Product.objects.get(pk=self.product.pk).price, Decimal('13.69'))
        self.assertTrue(Product.objects.get(pk=self.product.pk).image_origin)
        self.assertEqual(Product.objects.get(pk=self.product.pk).total_sold, 0)
        self.assertEqual(Product.objects.get(sku=self.product_old.sku).items_in_stock, 0)
        Product.objects.all().delete()

    def test_upload_file(self):
        resp_error = self.app.get(reverse('upload_csv'))
        self.assertEqual(resp_error.status_code, 302)
        resp = self.app.get(reverse('upload_csv'), user=self.user)
        self.assertEqual(resp.status_code, 200)
        form = resp.forms['upload_form']
        upload_file = open(self.file_csv)
        form['csvfile'] = (upload_file.name, upload_file.read())
        form.submit()
        self.assertEqual(CsvFile.objects.all().count(), 1)
        self.assertEqual(Product.objects.all().count(),19)
        Product.objects.all().delete()



