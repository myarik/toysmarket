# -*- coding: utf-8 -*-
from django import template
from toysmarket.apps.product.models import Category

register = template.Library()


@register.inclusion_tag('catalog_list.html')
def catalog_list():
    return {'root_categories': Category.objects.root_categories()}
