# -*- coding: utf-8 -*-
import csv
import os

import logging
import traceback

from toysmarket.apps.core.error_exeption import ValidateError

from toysmarket.apps.product.models import Product, CsvFile
from toysmarket.apps.product.utils import get_category

from toysmarket.settings.utils import root
from django.conf import settings

from django.core.files import File

from decimal import Decimal


logger = logging.getLogger('upload_csv')


class UploadProduc(object):
    """
        Class wich upload product from csv file to database
        _list_field - list fileld in csv file
    """
    _list_field = (
        'sku',
        'name',
        'short_description',
        'active',
        'items_in_stock',
        'price',
        'image_origin',
        'image_small',
        'category',
        'valuta',
        'description',
        'sale',
    )

    def __init__(self, csvfile_path, STATUS):
        self.csvfile = root() + csvfile_path
        self.STATUS = STATUS
        self.check()

    def check(self):
        if not os.path.isfile(self.csvfile):
            raise ValidateError('File %s not found' % self.csvfile)
        if self.STATUS not in [status for status, v in CsvFile.UPLOAD_STATUS]:
            raise ValidateError('Incorrect process status')

    def unicode_csv_reader(self, dialect=csv.excel, **kwargs):
        with open(self.csvfile, 'rb') as csv_data:
            csv_reader = csv.reader(csv_data, dialect=dialect, **kwargs)
            for i, row in enumerate(csv_reader):
                if i < 2:
                    continue
                yield [unicode(cell, 'utf-8') for cell in row]

    def load_from_list(self, ilist):
        """
            Args ilist:
            list from csv file
        Return:
            dict with all row
        """

        idict = {}
        for num, item in enumerate(self._list_field):
            idict[item] = ilist[num]
        return  idict

    def get_images(self, image):
        """
        Args image:
            path to image
        Return:
            Image
        """
        if not os.path.isdir(settings.IMAGES_DIR):
            os.makedirs(settings.IMAGES_DIR)
            return None
        image_path = os.path.join(settings.IMAGES_DIR, image)
        if os.path.isfile(image_path):
            try:
                img = open(image_path)
            except IOError as err:
                logger.info(
                    "Can't open file error",
                    {
                        'error': unicode(err),
                        'file': image_path
                    }
                )
                return
            return File(img)
        else:
            logger.info(
                "File not exist error",
                {
                    'file': image_path
                }
            )
            return None

    def prepare_dict(self, idict):
        """
        Args idict:
            Dict with all value
        Return:
            Dict with prepare date
        """
        if isinstance(idict.get('active', None), basestring):
            if idict['active'] == 'Y':
                idict['active'] = True
            else:
                idict['active'] = False
        else:
            idict['active'] = True

        if isinstance(idict.get('items_in_stock', None), basestring):
            try:
                idict['items_in_stock'] = int(idict['items_in_stock'])
            except:
                idict['items_in_stock'] = 0
        else:
            idict['items_in_stock'] = 0

        if isinstance(idict.get('price', None), basestring):
            idict['price'] = Decimal(idict['price']).quantize(Decimal('.01'))
        else:
            idict['price'] = Decimal(0).quantize(Decimal('.01'))

        if isinstance(idict.get('category', None), basestring):
            idict['category'] = get_category(idict['category'])
        else:
            idict['category'] = None

        SET_VALUE = False

        if isinstance(idict.get('valuta', None), basestring):
            for value, item  in Product.VALUE_STATUS:
                if item == idict['valuta']:
                    idict['valuta'] = value
                    SET_VALUE = True
                    break

        if not SET_VALUE:
            idict['valuta'] = Product.UA

        if isinstance(idict.get('sale', None), basestring):
            if idict['sale'] == 'Y':
                idict['sale'] = True
            else:
                idict['sale'] = False
        else:
            idict['sale'] = False

        NOT_IMAGE = True

        if isinstance(idict.get('image_origin', None), basestring):
            idict['image_origin'] = self.get_images(idict['image_origin'])
            if idict['image_origin']:
                NOT_IMAGE = False

        if NOT_IMAGE:
            try:
                del(idict['image_origin'])
            except KeyError:
                pass
        try:
            del(idict['image_small'])
        except KeyError:
            pass

        idict['total_sold'] = 0
        idict['allow_buy'] = True

        return idict

    def clean_image_directory(self):
        '''Clear images folder'''
        command = 'rm -rf %s/*' % settings.IMAGES_DIR
        os.system(command)

    def reload_databas(self):
        """
        Remove all data from table product
        """
        Product.objects.all().delete()

    def product_list(self):
        """
        Return:
            list - All products list
        """
        return list(Product.objects.all().values_list('sku', flat=True))

    def insert_data(self, idict):
        """
        Insert or update model Prosuct
        Args:
            idict - dict with fields
        """
        try:
            _product = Product.objects.get(sku=idict['sku'])
            try:
                new_image = idict['image_origin']
                if idict.get('image_origin'):

                    old_image = _product.image_origin
                    if old_image:
                        try:
                            old_image.storage.delete(old_image.name)
                        except Exception as err:
                            logger.info(
                                "Error delete image",
                                {
                                    'error': unicode(err),
                                    'value': idict
                                }
                            )
                    try:
                        _product.image_origin.save(
                            os.path.basename(new_image.name), new_image)
                        del(idict['image_origin'])
                    except Exception as err:
                        logger.info(
                            "Error update image",
                            {
                                'error': unicode(err),
                                'value': idict
                            }
                        )
                Product.objects.filter(sku=idict['sku']).update(**idict)
                _product.category.clean_cache()
            except Exception as err:
                logger.info(
                    "Error update product",
                    {
                        'error': unicode(err),
                        'value': idict
                    }
                )
                return False
        except Product.DoesNotExist:
            try:
                Product.objects.create(**idict)
            except:
                logger.info(
                    "Error create product",
                    {
                        'error': unicode(err),
                        'value': idict
                    }
                )
                return False
        return True

    def to_exclude(self, first_list, second_list):
        """
        Return differents list
         Args:
            (list): first_list
            (list): second_list
        """
        return list(
            set(first_list) - set(second_list)
        )

    def process(self):
        """
            Check status run process
        """

        product_sku = []
        product_old_list = []
        if self.STATUS == CsvFile.RELOAD:
            self.reload_databas()
        else:
            product_old_list = self.product_list()
        for row in self.unicode_csv_reader():
            if row:
                idict = self.load_from_list(row)
                if self.insert_data(self.prepare_dict(idict)):
                    product_sku.append(idict.get('sku'))
        if product_old_list:
            update_sku = self.to_exclude(product_old_list, product_sku)
            if self.STATUS == CsvFile.UPDATE_REMOVE:
                Product.objects.filter(sku__in=update_sku).update(active=False)
            elif self.STATUS == CsvFile.UPDATE_DISABLED:
                Product.objects.filter(sku__in=update_sku).update(items_in_stock=0, allow_buy=False)
        self.clean_image_directory()

