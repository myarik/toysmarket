# -*- coding: utf-8 -*-
import os

from optparse import make_option
from django.core.management import BaseCommand

from openpyxl import load_workbook

import re
from toysmarket.apps.product.models import Category


class Command(BaseCommand):
    """
    Create list of category
    Remove all exist category
    Params:
        --filepath - give path to xls file
    """
    help = "Create Category in DataBase"
    option_list = BaseCommand.option_list + (
        make_option(
            '--filepath',
            dest='filepath',
            default=None,
            help='Update table category'
        ),
    )

    def handle(self, filepath, *args, **options):
        filepath = filepath
        if not filepath:
            print "Enter file path"
            return
        if not os.path.isfile(filepath):
            print "Incorrect path on file %s" % filepath
            return
        Category.objects.all().delete()
        wb = load_workbook(filename=filepath, use_iterators=True)
        ws = wb.get_sheet_by_name(name=u'Лист1')
        parent_obj = None
        for row in ws.iter_rows():
            list_value = []
            for cell in row:
                if cell.row == 1:
                    continue
                try:
                    list_value.append(cell.internal_value.encode('utf-8'))
                except AttributeError:
                    list_value.append(int(cell.internal_value))
            if list_value:
                type_category = re.search(ur'^((\s)?__\|(\d)\|)',list_value[1]).group(3)
                categroy_name = re.search(ur'[^(__\|\d\|)].*',list_value[1]).group()
                categroy_name = unicode(re.sub(r'^\xc2\xa0', '', categroy_name), 'utf-8')
                if type_category == '1':
                    parent_obj, create_status = Category.objects.get_or_create(name=categroy_name,
                                                                               uid=int(list_value[2]))
                else:
                    Category.objects.get_or_create(name=categroy_name,uid=int(list_value[2]), parent=parent_obj)
                print "Add %s" % categroy_name
