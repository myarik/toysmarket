# -*- coding: utf-8 -*-
from django.db import models
from django.core.cache import cache


class ProductManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(active=True, category__is_active=True, **kwargs)

    def aviable_for_bay(self, **kwargs):
        return self.filter(
            active=True,
            category__is_active=True, **kwargs).exclude(items_in_stock=0)


class CategoryManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(is_active=True, **kwargs)

    def root_categories(self, **kwargs):
        return filter(lambda x: x.is_root_node(), self.active(**kwargs))
