# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from django.contrib import messages

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django.shortcuts import redirect

from django.views.generic import CreateView, TemplateView, DetailView, ListView

from toysmarket.apps.core.error_exeption import ValidateError
from toysmarket.apps.core.search import SphinxSearcher

from toysmarket.apps.product.models import CsvFile, Category, Product
from toysmarket.apps.product.upload_product import UploadProduc

from toysmarket.apps.extra_settings.models import HitToys, NewToys, SaleToys

from sortable_listview import SortableListView

from toysmarket.apps.order.core.storage import RedisOrderStorage

import logging
import traceback

logger = logging.getLogger('upload_csv')


class UploadCsvFile(CreateView):
    model = CsvFile
    fields = ['uploading_status', 'csvfile']
    template_name = 'upload_csvfile.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            messages.info(request, _("Доступ к возможностям сайта ограничен"))
            return redirect(reverse('error_message'))
        return super(UploadCsvFile, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        try:
            upload = UploadProduc(
                csvfile_path=self.object.csvfile.url,
                STATUS=self.object.uploading_status
            )
            upload.process()
        except ValidateError as err:
            logger.info(
                'Upload csvfile error',
                {
                    'error': unicode(err),
                    'traceback': traceback.format_exc()
                }
            )
            messages.error(self.request, unicode(err))
            return redirect(reverse('error_message'))

        return redirect('/admin/')


class IndexView(TemplateView):
    """
    Index Page
    """
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['product_hit'] = HitToys.get_slides()
        context['product_new'] = NewToys.get_slides()
        context['product_sale'] = SaleToys.get_slides()
        return context


class ProductListView(SortableListView):
    """
        Product List View
    """
    allowed_sort_fields = {'name': {'default_direction': '',
                                    'verbose_name': u'Название'},
                           'price': {'default_direction': '',
                                     'verbose_name': u'Цена'},
                           'date_added': {'default_direction': '',
                                          'verbose_name': u'Поступление'}}
    default_sort_field = 'name'
    paginate_by = 20
    template_name = 'list.html'
    model = Product
    catalog_name = 'Каталог игрушек'
    catalog = None

    def get_queryset(self, **kwargs):
        slug = self.kwargs.get('slug')
        if slug:
            product_list = []
            for catalog in Category.objects.filter(
                    slug=self.kwargs.get('slug')):
                [product_list.append(product)
                    for product in catalog.get_products()]
            self.product_amount = len(product_list)
            self.catalog_name = catalog.name
            self.catalog = catalog
            if self.sort == u'price':
                return sorted(product_list, key=lambda product: product.price)
            if self.sort == u'-price':
                return sorted(
                    product_list,
                    key=lambda product: product.price,
                    reverse=True)
            if self.sort == u'date_added':
                return sorted(
                    product_list, key=lambda product: product.date_added)
            if self.sort == u'-date_added':
                return sorted(
                    product_list, key=lambda product: product.date_added,
                    reverse=True)
            if self.sort == u'-name':
                return product_list[::-1]
            return product_list
        else:
            qs = super(ProductListView, self).get_queryset(**kwargs)
            self.product_amount = len(qs)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        crumb = [('Каталог игрушек', reverse('product_list'))]
        if self.catalog:
            if self.catalog.parent:
                crumb.append(
                    (
                        self.catalog.parent.name,
                        self.catalog.parent.get_absolute_url()
                    )
                )
            crumb.append(
                (self.catalog.name, self.catalog.get_absolute_url()))
            context['catalog_id'] = self.catalog.id
        context['catalog_name'] = self.catalog_name
        context['product_amount'] = self.product_amount
        context['catalog_tree'] = crumb
        order = RedisOrderStorage(self.request).get_from_storage()
        if order:
            id_dict = {}
            for item in order.get_info().get('order_items'):
                id_dict[item.get('product')] = item.get('quantity')
            for object_item in context['object_list']:
                if object_item.pk in order.get_product_list_id():
                    object_item.count = id_dict[object_item.pk]
                    object_item.basket_class = 'on_basket'
        return context


class ProductDetailView(DetailView):
    """
    Purchase detail view with list of products
    """
    model = Product
    template_name = 'product_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        order = RedisOrderStorage(self.request).get_from_storage()
        if order:
            if context['object'].pk in order.get_product_list_id():
                for item in order.get_info().get('order_items'):
                    if item.get('product') == context['object'].pk:
                        context['object'].count = item.get('quantity')
        return context


class SearchListView(ListView):
    """SearchListView"""

    paginate_by = 20
    template_name = 'search.html'

    def get_queryset(self, **kwargs):
        qs = SphinxSearcher()
        query = self.request.GET.get('query', '')
        min_price = self.request.GET.get('min_price', '')
        max_price = self.request.GET.get('max_price', '')
        category_id = self.request.GET.get('catalog_id', None)

        if min_price:
            if not min_price.isdigit():
                min_price = 0
        if max_price:
            if not max_price.isdigit():
                min_price = 100000
        range_price = None
        if min_price or max_price:
            if not min_price:
                range_price = (0, max_price)
            elif not max_price:
                range_price = (min_price, 10000)
            else:
                range_price = (min_price, max_price)
        if category_id:
            category = Category.objects.get(pk=category_id)
            if not category.is_leaf_node():
                category_id = [i.pk for i in category.get_children()]

        ids = qs.search(
            search_word=query, range_filters=range_price,
            category=category_id
        )
        self.search_count = len(ids)
        return Product.objects.filter(id__in=ids)

    def get_context_data(self, **kwargs):
        context = super(SearchListView, self).get_context_data(**kwargs)
        queries_without_page = self.request.GET.copy()
        if queries_without_page.has_key('page'):
            del queries_without_page['page']
        context['queries'] = queries_without_page
        context['result_count'] = self.search_count
        return context
