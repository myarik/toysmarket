# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from .models import CsvFile


class CsvFileForm(forms.ModelForm):

    class Meta:
        model = CsvFile
