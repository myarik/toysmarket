# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from toysmarket.apps.product.views import (IndexView, ProductListView,
                                           ProductDetailView, SearchListView)

urlpatterns = patterns('',
                       url(r'^$', IndexView.as_view(), name='index'),
                       url(r'^catalog/(?P<slug>([-\w]+)*)?$',
                           ProductListView.as_view(),
                           name='product_list'),
                       url(r'^goods/(?P<slug>[-\w]+)-(?P<good_id>\d+)/$',
                           ProductDetailView.as_view(),
                           name='product_details'),
                       url(r'^search/$', SearchListView.as_view(),
                           name='search'),
)
