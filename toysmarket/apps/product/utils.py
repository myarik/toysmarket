# -*- coding: utf-8 -*-
from toysmarket.apps.product.models import Category

__author__ = 'yarik'

def get_category(category_uid):
    """
        Return category given id
    """
    try:
        category_uid = int(category_uid)
    except:
        return Category.objects.get(uid=0)
    try:
        return Category.objects.get(uid=category_uid)
    except Category.DoesNotExist:
        return Category.objects.get(uid=0)

