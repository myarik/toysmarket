# -*- coding: utf-8 -*-
from django.contrib import admin
from django.template import Template
from django.template.context import Context

from .models import Category, Product, CsvFile

from toysmarket.apps.core.fields import AdminImageWidget

# class CsvFileAdmin(admin.ModelAdmin):
    #     """CsvFileAdmin"""
#     list_display = ['upload_date']
#     readonly_fields = ('upload_date', 'csvfile')


# admin.site.register(CsvFile, CsvFileAdmin)

class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = (
        'name',
        'slug',
        'sku',
        'price',
        'category',
        'items_in_stock',
        'image_column'
    )
    search_fields = ('name', 'sku')

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image_origin':
            kwargs.pop("request")
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(ProductAdmin, self).formfield_for_dbfield(
            db_field,
            **kwargs
        )

    def image_column(self, obj):
        template = Template("""
            {% if image_origin %}
                <a href='{{image_origin}}'><img src="{{  image }}" height="100"/></a>
            {% endif %}
        """)
        return template.render(Context({
            'image_origin': obj.image_origin.url if obj.image_origin else None,
            'image': obj.image_thumbnail.url if obj.image_origin else None,
        }))
    image_column.allow_tags = True
    image_column.short_description = u'Картинка'

admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
