# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

from django.conf import settings

from django.core.cache import cache
from pytils.translit import slugify

from django.utils.encoding import force_unicode

from django.core.urlresolvers import reverse

from imagekit.processors import resize
from imagekit.models.fields import ImageSpecField

from .manager import ProductManager, CategoryManager


class Category(MPTTModel):
    """
        A Category
        name
            The name of the category.
        slug
            Part of the URL
        parent
            Parent of the category. This is used to create a category tree. If
            it's None the category is a top level category.
        description
            The description of the category. This can be used in details views
            of the category.
        is_active_
            If True enabled, Flase Disabled
        meta
            Meta keywords of the category
        path
            Url path
        uid
            The unique id of the category
    """

    name = models.CharField(_('Название'), max_length=255,
                            help_text=_(
                                'При изменении названия удалите \
записи в полях "Ссылка" и "Абсолютная ссылка"'))
    slug = models.SlugField(_('Ссылка'), blank=True, null=True,
                            help_text=_("Used for URLs, auto-generated from name if blank"),
                            )
    meta = models.TextField(_("Meta Description"), blank=True, null=True,
                            help_text=_("Meta description for this category"))
    description = models.TextField(_("Описание"), blank=True, null=True,
                                   help_text="Optional")

    is_active = models.BooleanField(_("Статус"), default=True)

    path = models.URLField(_('Абсолютная ссылка'), blank=True, null=True, max_length=255)

    parent = TreeForeignKey('self',
                            blank=True,
                            null=True,
                            related_name="children",
                            verbose_name='Родитель')

    uid = models.IntegerField(max_length=50, unique=True)

    objects = CategoryManager()

    class Meta:
        verbose_name = _('Категория')
        verbose_name_plural = _('Категории')
        ordering = ("name", )

    def __unicode__(self):
        return "%s" % (self.name)

    def get_path(self):
        ancestors = []
        if self.parent:
            ancestors = list(self.parent.get_ancestors(include_self=True))
        ancestors = ancestors + [self, ]
        return ancestors

    def join_path(self, joiner, field, ancestors):
        return joiner.join([force_unicode(getattr(i, field)) for i in ancestors])

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        ancestors = self.get_path()
        self.path = self.join_path(u'/', 'slug', ancestors)
        # cache_key = "cat-%s" % self.site.id
        # cache.delete(cache_key)
        update_descendants = bool(self.pk)
        super(Category, self).save(*args, **kwargs)

        if update_descendants:
            children = list(self.children.all())
            for child in children:
                child.is_active = self.is_active
                child.save()
            if self.parent:
                cache_key = "%s-%s-category-children" % (
                    settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                    self.parent.id)
                cache.delete(cache_key)
                self.clean_cache()

    def get_absolute_url(self):
        return reverse('product_list',  kwargs={'slug': self.slug})

    def get_children(self):
        """
            Returns child categories of the category
        """
        cache_key = "%s-%s-category-children" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)

        children = cache.get(cache_key)

        if children is not None:
            return children

        children = list(self.children.filter(is_active=True))

        cache.set(cache_key, children)
        return children

    def get_parents(self):
        """
        Returns all parent categories.
        """
        cache_key = "%s-%s-category-parents" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)

        parents = cache.get(cache_key)

        if parents is not None:
            return parents

        parents = self.parent
        cache.set(cache_key, parents)

        return parents

    def get_products(self):
        """
        Returns the direct products of the category.
        """
        cache_key = "%s-%s-category-products" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        products = cache.get(cache_key)
        if products is not None:
            return products
        products = list(self.products.filter(active=True))
        for child in self.get_children():
            [products.append(item) for item in list(child.products.filter(active=True))]
        cache.set(cache_key, products)
        return products

    def clean_cache(self):
        keys = ["%s-%s-category-products" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)]
        if self.parent:
            [keys.append("%s-%s-category-products" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, item.id))
                for item in self.parent.get_ancestors(include_self=True)]
        cache.delete_many(keys)


class Product(models.Model):
    UA = 0
    USD = 1
    EUR = 2

    VALUE_STATUS = (
        (UA, _("UA")),
        (USD, _("USD")),
        (EUR, _("EUR")),
    )
    """
    name
        The name of the product

    slug
        Part of the URL

    sku
        The external unique id of the product

    price
        The gross price of the product

    image_small
        Image small

    image_big
        Image big

    short_description
        The short description of the product. This is used within overviews.

    description
        The description of the product. This is used within the detailed view
        of the product.

    valuta
        valuta

    sale
        True or False

    date_added
        Add product date

    active
        True or False

    items_in_stock
        Total products

    total_sold
        Total pay

    """
    name = models.CharField(_("Название"),
                            help_text=_(u"The name of the product."),
                            max_length=255,
                            blank=True)
    slug = models.SlugField(_("Ссылка"),
                            help_text=_(u"The unique last part of the Product's URL."),
                            max_length=255)
    sku = models.CharField(_("Артикул"), max_length=255, blank=True, null=True,
                           help_text=_("Defaults to slug if left blank"))

    price = models.DecimalField(_("Цена за шт."), max_digits=19, decimal_places=2)
    image_origin = models.ImageField(upload_to='images', blank=True, null=True,
                                     verbose_name=_('Картинка'))
    image_thumbnail = ImageSpecField([resize.ResizeToFit(280, 160)],
                                     source='image_origin', format='JPEG', options={'quality': 100})
    short_description = models.TextField(_("Краткое описание"),
                                         help_text=_("This should be a 1 or 2 line description \
in the default site language for use in product listing screens"), max_length=200, default='', blank=True)
    description = models.TextField(_("Полное описание"), default='', blank=True)
    category = models.ForeignKey(Category,
                                 blank=True,
                                 verbose_name=_("Категория"),
                                 related_name='products')
    valuta = models.PositiveSmallIntegerField(_('Валюта'),
                                              choices=VALUE_STATUS,
                                              default=UA)
    items_in_stock = models.DecimalField(_("Единиц товара в продаже"),
                                         max_digits=18, decimal_places=6, default='0')
    total_sold = models.DecimalField(_("Всего проданно"),  max_digits=18, decimal_places=6, default='0')
    sale = models.BooleanField(_("Акция"), default=False)
    meta = models.TextField(_("Meta Description"),
                            max_length=200, blank=True, null=True,
                            help_text=_("Meta description for this product"))
    date_added = models.DateField(_("Дата добавления"), auto_now_add=True)
    active = models.BooleanField(_("Статус"), default=True, help_text=_("This will determine whether or not this product will appear on the site"))
    ordering = models.IntegerField(_("Ordering"), default=0, help_text=_("Override alphabetical order in category display"))
    allow_buy = models.BooleanField(
        _("Есть в наличии"),
        default=True, help_text=_("This will disabled product on site"))

    objects = ProductManager()

    class Meta:
        verbose_name = _('Товары')
        verbose_name_plural = _('Товары')
        ordering = ("name", "items_in_stock")

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.slug)

    def get_absolute_url(self):
        return reverse('product_details', kwargs={'slug': self.slug,
                                                  'good_id': self.id})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)
        self.category.clean_cache()


class CsvFile(models.Model):
    """
    RELOAD - clean database
    UPDATE_REMOVE - Disable on site
    UPDATE_DISABLED - items_in_stock = o

    uploading_type
        Choices how upload
    csvfile
        FileCSV
    upload_date
        Date Upload
    """
    RELOAD = 0
    UPDATE_REMOVE = 1
    UPDATE_DISABLED = 2

    UPLOAD_STATUS = (
        (RELOAD, _("Перезалить базу(удалит не существующие записи)")),
        (UPDATE_REMOVE, _("Обновить базу и удалить с сайта товары которых нет в базе")),
        (UPDATE_DISABLED, _("Обновить базу и отключить товары которых нет в базе")),
    )
    uploading_status = models.PositiveSmallIntegerField(_('Тип обновления'),
                                                        choices=UPLOAD_STATUS,
                                                        default=UPDATE_DISABLED)
    csvfile = models.FileField(upload_to="csvfile/", verbose_name=_('CSV Файл'))
    upload_date = models.DateField(_("Дата Загрузки"), auto_now_add=True)

    class Meta:
        verbose_name = _('CSV Файл')
        verbose_name_plural = _('CSV Файл')
        ordering = ("upload_date", )

    def __unicode__(self):
        return "%s" % self.upload_date

    def save(self, *args, **kwargs):
        update_descendants = bool(self.pk)
        super(CsvFile, self).save(*args, **kwargs)

        if not update_descendants:
            while CsvFile.objects.all().count() > 10:
                CsvFile.objects.all()[0].delete()


@receiver(post_delete, sender=Product, dispatch_uid='product_post_delete')
def productimage_post_delete(sender, instance, **kwargs):
    '''
    This function deletes files from storage
    '''
    image = instance.image_origin
    thumbnail = instance.image_thumbnail
    try:
        thumbnail.storage.delete(thumbnail.name)
        image.storage.delete(image.name)
    except:
        pass
