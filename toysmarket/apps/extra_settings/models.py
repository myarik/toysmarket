# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext as _

from django.db.models.signals import pre_save
from django.conf import settings

from django.db.models import F
from django.core.cache import cache

from ckeditor.fields import RichTextField

from toysmarket.apps.product.models import Product


class FlatpagesNav(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Заголовок'))
    link = models.CharField(max_length=255,
                            verbose_name=_('Ссылка'),
                            blank=True, null=True)
    flatpage = models.ForeignKey(FlatPage,
                                 verbose_name=_('Страница'),
                                 blank=True, null=True)
    order = models.PositiveIntegerField(default=0,
                                        blank=False, null=False,
                                        verbose_name=_('Порядок'))

    class Meta:
        ordering = ('order',)
        verbose_name = _('Меню')
        verbose_name_plural = _('Меню')

    def __unicode__(self):
        return "%s" % self.order


class ExtraInfo(models.Model):
    kyivstar_phone = models.CharField(
        max_length=25, verbose_name=_('Телефон_Kyivstar'),
        blank=True, null=True)
    mts_phone = models.CharField(
        max_length=25, verbose_name=_('Телефон_MTC'), blank=True, null=True)
    life_phone = models.CharField(
        max_length=25, verbose_name=_('Телефон_life'), blank=True, null=True)
    footer_text = RichTextField(
        verbose_name=_('Описание в низу страницы'), blank=True, null=True)
    skype_login = models.CharField(
        max_length=50, verbose_name=_('Skype login'), blank=True, null=True)
    index_text = RichTextField(
        verbose_name=_('Описание на главной'), blank=True, null=True)
    email_admin = models.EmailField(
        verbose_name=u'Почтовый адрес администратора', blank=True, null=True)
    price_limit = models.DecimalField(
        max_digits=19, decimal_places=2,
        verbose_name=_('Минимальная сумма заказа'), default='500.00')

    class Meta:
        verbose_name = _('Дополнительная Информация')
        verbose_name_plural = _('Дополнительная Информация')

    def __unicode__(self):
        return u"Редактировать дополнительную информацию"

    def save(self, *args, **kwargs):
        '''
        Method is overridden to always had only one object.
        When creating a new object, the old object is overwritten.
        '''
        if self.pk == 1:
            return super(ExtraInfo, self).save(*args, **kwargs)
        try:
            obj = ExtraInfo.objects.get(pk=1)
        except ExtraInfo.DoesNotExist:
            return super(ExtraInfo, self).save(*args, **kwargs)
        obj.kyivstar_phone = self.kyivstar_phone
        obj.mts_phone = self.mts_phone
        obj.life_phone = self.life_phone
        obj.footer_text = self.footer_text
        obj.skype_login = self.skype_login
        obj.index_text = self.index_text
        obj.save()
        self = obj


class Slider(models.Model):
    sku = models.CharField(_("Артикул"), max_length=255,
                           help_text=_("Введите артикуль игрушки"))
    order = models.PositiveSmallIntegerField()

    class Meta:
        abstract = True

    def __unicode__(self):
        return 'Артикуль: %s Порядок: %s' % (self.sku, self.order)

    # Get all slides
    @classmethod
    def get_slides(self):
        cache_key = "%s-%s" % (
                    settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                    self.__name__
        )
        slide_list = cache.get(cache_key)

        if slide_list is not None:
            return slide_list
        sku_list = self.objects.all().values_list('sku', flat=True)
        slide_list = Product.objects.filter(sku__in=sku_list)
        cache.set(cache_key, slide_list)
        return slide_list

    @classmethod
    def clean_cache(self):
        cache_key = "%s-%s" % (
                    settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                    self.__name__
        )
        cache.delete(cache_key)

    @classmethod
    def adjust_order(cls, sender, instance, *args, **kwargs):
        """
            If the order of the new slide is not unique then

            successive slides (according to order) will be shifted:

            their order fields will be incremented.


            Example:

                Have slides with orders: 1,2,5,6

                New slide order: 1

                Result: 1,2,3,5,6

                (New slide is at the first place)
        """
        cls.clean_cache()
        if not kwargs['raw']:
            order = instance.order
            if HitToys.objects.filter(order=order).exists():
                succ_slides = HitToys.objects.filter(order__gte=order)
                prev = order
                last = None
                for slide in succ_slides:
                    if slide.order > prev + 1:
                        last = slide.order
                        break
                    else:
                        prev = slide.order
                if last:
                    succ_slides = succ_slides.filter(order__lt=last)
                succ_slides.update(order=F('order') + 1)


class HitToys(Slider):
    class Meta:
        verbose_name = _('Самые популярные игрушки')
        verbose_name_plural = _('Самые популярные игрушки')


class SaleToys(Slider):
    class Meta:
        verbose_name = _('Распродажи')
        verbose_name_plural = _('Распродажи')


class NewToys(Slider):
    class Meta:
        verbose_name = _('Новинки')
        verbose_name_plural = _('Новинки')


pre_save.connect(HitToys.adjust_order, HitToys)
pre_save.connect(SaleToys.adjust_order, SaleToys)
pre_save.connect(NewToys.adjust_order, NewToys)
