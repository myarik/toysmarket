# -*- coding: utf-8 -*-
from django import template
from toysmarket.apps.extra_settings.models import FlatpagesNav

register = template.Library()


@register.inclusion_tag('menu.html')
def menu():
    menu = FlatpagesNav.objects.all()
    return {'menu': menu}
