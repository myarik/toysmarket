# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from .models import HitToys, SaleToys, NewToys

from toysmarket.apps.product.models import Product


class SliderForm(forms.ModelForm):

    def clean_sku(self):
        if not Product.objects.filter(sku=self.cleaned_data['sku']):
            raise forms.ValidationError(
                u'Такого артыкуля не существует')
        return self.cleaned_data['sku']


class HitToysForm(SliderForm):
    def clean_sku(self):
        super(HitToysForm, self).clean_sku()
        if HitToys.objects.filter(sku__iexact=self.cleaned_data['sku']).exclude(id=self.instance.id):
            raise forms.ValidationError(
                u'Слайд с таким артыкулем уже существует'
            )
        return self.cleaned_data['sku']

    class Meta:
        model = HitToys


class SaleToysForm(SliderForm):
    def clean_sku(self):
        super(SaleToysForm, self).clean_sku()
        if SaleToys.objects.filter(sku__iexact=self.cleaned_data['sku']).exclude(id=self.instance.id):
            raise forms.ValidationError(
                u'Слайд с таким артыкулем уже существует'
            )
        return self.cleaned_data['sku']

    class Meta:
        model = SaleToys


class NewToysForm(SliderForm):
    def clean_sku(self):
        super(NewToysForm, self).clean_sku()
        if NewToys.objects.filter(sku__iexact=self.cleaned_data['sku']).exclude(id=self.instance.id):
            raise forms.ValidationError(
                u'Слайд с таким артыкулем уже существует'
            )
        return self.cleaned_data['sku']

    class Meta:
        model = NewToys
