# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.flatpages.models import FlatPage

from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld
from toysmarket.apps.extra_settings.models import (FlatpagesNav, ExtraInfo,
                                                   HitToys, SaleToys, NewToys)

from django import forms
from ckeditor.widgets import CKEditorWidget

from forms import HitToysForm, SaleToysForm, NewToysForm


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = FlatPage


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


class FlatpagesNavAdminForm(forms.ModelForm):
    class Meta:
        model = FlatpagesNav


class FlatpagesNavAdmin(admin.ModelAdmin):
    form = FlatpagesNavAdminForm
    list_display = ('title', 'order')


class HitToysAdmin(admin.ModelAdmin):
    form = HitToysForm


class SaleToysAdmin(admin.ModelAdmin):
    form = SaleToysForm


class NewToysAdmin(admin.ModelAdmin):
    form = NewToysForm

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(FlatpagesNav, FlatpagesNavAdmin)
admin.site.register(ExtraInfo)
admin.site.register(HitToys, HitToysAdmin)
admin.site.register(SaleToys, SaleToysAdmin)
admin.site.register(NewToys, NewToysAdmin)
