# -*- coding: utf-8 -*-
from toysmarket.apps.extra_settings.models import ExtraInfo


def phone_info(request):
    responce_dict = {}
    phone_info = ExtraInfo.objects.filter(pk=1)
    if phone_info:
        obj = phone_info[0]
        responce_dict['phone_kyivstar'] = obj.kyivstar_phone if obj.kyivstar_phone \
            else None
        responce_dict['phone_life'] = obj.life_phone if obj.life_phone \
            else None
        responce_dict['phone_mts'] = obj.mts_phone if obj.mts_phone \
            else None
        responce_dict['footer_text'] = obj.footer_text if obj.footer_text \
            else None
        responce_dict['skype_login'] = obj.skype_login if obj.skype_login \
            else None
        responce_dict['index_text'] = obj.index_text if obj.index_text \
            else None
        responce_dict['price_limit'] = obj.price_limit if obj.price_limit \
            else None
    else:
        responce_dict['phone_kyivstar'] = None
        responce_dict['phone_life'] = None
        responce_dict['phone_mts'] = None
        responce_dict['footer_text'] = None
        responce_dict['skype_login'] = None
        responce_dict['index_text'] = None
        responce_dict['price_limit'] = None
    return responce_dict
