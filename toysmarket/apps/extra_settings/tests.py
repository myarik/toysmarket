# -*- coding: utf-8 -*-

from django_webtest import WebTest
from model_mommy import mommy

from django.core.cache import cache

from toysmarket.apps.extra_settings.models import HitToys, SaleToys
from toysmarket.apps.product.models import Product, Category


class ModelTest(WebTest):

    def setUp(self):
        self.category = mommy.make(Category, name=u"Boys", uid=1)
        self.pr1 = mommy.make(Product, sku='123', category=self.category)
        self.pr2 = mommy.make(Product, sku='321', category=self.category,)
        self.hit_slide = mommy.make(HitToys, sku='123')
        self.sale_slide = mommy.make(SaleToys, sku='123')
        cache.clear()

    def test_model_hit(self):
        self.assertTrue(isinstance(self.hit_slide, HitToys))
        mommy.make(HitToys, sku='321')
        self.assertEqual(len(HitToys.get_slides()), 2)

    def test_model_sale(self):
        self.assertTrue(isinstance(self.sale_slide, SaleToys))
        mommy.make(SaleToys, sku='321')
        self.assertEqual(len(SaleToys.get_slides()), 2)

