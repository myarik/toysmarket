# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder


class JsonResponse(HttpResponse):
    """
    HttpResponse descendant, which return response with ``application/json`` mimetype.
    """
    def __init__(self, data):
        content = json.dumps(data, cls=DjangoJSONEncoder, ensure_ascii=False).encode('utf-8')
        super(JsonResponse, self).__init__(content=content, mimetype='application/json; charset=utf-8')


def ajax_request(allow_cors=False, no_cache=False):
    """
    If view returned serializable dict, returns JsonResponse with this dict as content.

    allow_cors adds cross-domain headers
    http://www.w3.org/TR/cors/

    example:

        @ajax_request
        def my_view(request):
            news = News.objects.all()
            news_titles = [entry.title for entry in news]
            return {'news_titles': news_titles}
    """
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            response = func(request, *args, **kwargs)
            if isinstance(response, dict):
                response = JsonResponse(response)
                return response
            else:
                return response
        return wrapper
    return decorator


def no_cache(func):
    """
    Update response headers so that it dont cache
    """
    def wrapper(request, *args, **kwargs):
        response = func(request, *args, **kwargs)
        response['Pragma'] = 'no-cache'
        response['Cache-Control'] = 'no-cache, no-store, must-revalidate'
        response['Expires'] = 0
        return response
    return wrapper


def post_required(func):
    """Decorator that returns an error unless request.method == 'POST'."""

    def post_wrapper(request, *args, **kwds):
        if request.method != 'POST':
            return HttpResponse('This requires a POST request.', status=405)
        return func(request, *args, **kwds)

    return post_wrapper
