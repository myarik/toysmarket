# -*- coding: utf-8 -*-
from django.conf import settings
from sphinxit.core.processor import Search


class SearchConfig(object):
    DEBUG = settings.DEBUG
    SEARCHD_CONNECTION = {
        'host': settings.SPHINX_HOST,
        'port': settings.SPHINX_PORT,
    }


class SphinxSearcher(object):
    """
        Sphinx searcher over SphinxQL
    """
    def __init__(self, config=SearchConfig):
        self.config = config
        self.last_error = None

    def search(self, search_word='', range_filters=None, category=None,
               index=settings.SPHINX_INDEX, select_field='id'):
        '''
            search_word: 'search world'
            range_filters: [(from, to), ...]
        '''
        ql_query = Search(indexes=[index, ], config=self.config)
        ql_query = ql_query.limit(0, 200)
        my_filter = {}
        if range_filters:
            my_filter['price__between'] = \
                [float(range_filters[0]), float(range_filters[1])]
        if category:
            if isinstance(category, list):
                my_filter['category_id__in'] = category
            else:
                try:
                    my_filter['category_id__eq'] = int(category)
                except ValueError:
                    pass
        ql_query = \
            ql_query.select(select_field).match(search_word).filter(**my_filter)
        try:
            res = ql_query.ask()
        except Exception:
            return []
        ids = [obj['id'] for obj in res['result']['items']]
        return ids
