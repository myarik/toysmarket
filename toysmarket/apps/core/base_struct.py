# -*- coding: utf-8 -*-


class BaseStruct(object):
    """
        base class for classes
    """
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if hasattr(self, k):
                setattr(self, k, v)
            else:
                raise AttributeError(
                    '%s got unexpected param "%s"' % (type(self), k))

    def get_field_list(self):
        '''
            return list of own fields
        '''
        res = []
        for fld in dir(self):
            if not fld.startswith('_') and not callable(getattr(self, fld)):
                res.append(fld)
        return res

    def get_info(self):
        '''
            dump content to dict
        '''
        res = {}
        for fld in self.get_field_list():
            res[fld] = getattr(self, fld)
        return res

    @classmethod
    def load_from_dict(cls, idict):
        '''
            create new instance from dict
        '''
        obj = cls()
        for fld in obj.get_field_list():
            if fld in idict:
                try:
                    setattr(obj, fld, idict[fld])
                except AttributeError:
                    continue
        return obj