# -*- coding: utf-8 -*-
class ValidateError(Exception):
    def __init__(self, message=None):
        self.message = message
        super(ValidateError, self).__init__(message)

