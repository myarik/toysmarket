# -*- coding: utf-8 -*-
from celery import current_app


current_app.send_task(
    'emails.send_email',
    args=['test', 'my@email.com', {'name': 'Vasys'}, 'My message'],
)
