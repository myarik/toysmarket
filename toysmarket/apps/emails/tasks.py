# -*- coding: utf-8 -*-
import celery

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from .html2text import html2text


@celery.task(name='emails.send_email')
def message_send(name, to_email, context=None, subject=None,
                 from_addr=None, cc='', bcc=''):
    """
    Example:
        message_send('test', 'my@email.com', {'name': 'Vasys'}, 'My message')
    """
    context = context or {}
    message = render_to_string('mail/{}.html'.format(name), context)

    subject = subject or render_to_string('mail/{}_subject.txt'.format(name),
                                          context)

    if isinstance(to_email, basestring):
        to_email = [to_email]

    cc = ','.join(cc) if isinstance(cc, list) else cc
    bcc = ','.join(bcc) if isinstance(bcc, list) else bcc
    if not from_addr:
        from_addr = getattr(settings, 'EMAIL_FROM_ADDR')

    email = EmailMultiAlternatives(
        subject,
        html2text(message),
        from_addr,
        to_email
    )
    email.attach_alternative(message, 'text/html')
    email.send()
