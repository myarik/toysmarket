# -*- coding: utf-8 -*-
from django_webtest import WebTest
from model_mommy import mommy
from nose.tools import raises

from toysmarket.apps.core.error_exeption import ValidateError

from toysmarket.apps.order.core.order import OrderStruct
from toysmarket.apps.order.core.orderitem import OrderItemStruct
from toysmarket.apps.order.core.storage import RedisOrderStorage

from toysmarket.apps.order.models import Order, OrderItem, Delivery

from toysmarket.apps.extra_settings.models import ExtraInfo

from django.conf import settings
from django.contrib.auth import get_user_model

from django.core.urlresolvers import reverse

from decimal import Decimal
from toysmarket.apps.product.models import Product, Category

import os

from uuid import uuid4


User = get_user_model()

def create_product(name, price, category):
    return mommy.make(
        Product,
        name=name,
        price=price,
        image_origin=mommy.mock_file_jpeg,
        category=category,
        items_in_stock=50,
    )


class FakeSession(object):
    def __init__(self):
        self.session_key = 'hrsn14agce0lucvb7ju3dvvwxacxsdxr'


class FakeRequest(object):
    def __init__(self):
        self.session = FakeSession()

class ModelTest(WebTest):

    def setUp(self):
        self.user = mommy.make(User, first_name='Alex', last_name='Kerd', email='ex@example.com')
        self.delivery = mommy.make(Delivery, title='Nova Poshta')
        self.category = mommy.make(Category, name=u"Boys", uid=1)
        self.product = create_product(
            name='Bus',
            price=200,
            category = self.category
        )
        self.order = mommy.make(
            Order,
            user=self.user,
            delivery_type=self.delivery,
            uid='aeg5aeP4',
            status=Order.PROCESS
        )
        self.orderitem = mommy.make(
            OrderItem, order=self.order, product=self.product, quantity=2)
        mommy.make(ExtraInfo, price_limit=1)


    def test_model_orderitem(self):
        self.assertTrue(isinstance(self.orderitem, OrderItem))
        self.assertEqual(self.orderitem.product.name, u'Bus')
        self.assertEqual(self.orderitem.get_item_price(), Decimal(400.00))

    def test_mode_order(self):
        self.assertTrue(isinstance(self.order, Order))
        product_2 = create_product(
            name='Car Mak',
            price=50,
            category = self.category
        )
        mommy.make(OrderItem, order=self.order, product=product_2, quantity=5)
        self.assertEqual(self.order.get_order_price(), Decimal(650.00))

    def test_user_order(self):
        self.assertEqual(self.user.userorder.all()[0], self.order)


class OrderStructTest(WebTest):

    def setUp(self):
        self.user = mommy.make(
            User, first_name='Alex', last_name='Kerd', email='ex@example.com')
        self.delivery = mommy.make(Delivery, title='Nova Poshta')
        self.category = mommy.make(Category, name=u"Boys", uid=1)
        self.product = create_product(
            name='Bus',
            price=200,
            category=self.category
        )
        self.order_item_list = [{
            'product': self.product.id,
            'quantity': '15'
        }]
        mommy.make(ExtraInfo, price_limit=1)

    @raises(ValidateError)
    def test_orderitem_validate_id(self):
        _dict = {
            'product': 'as',
            'quantity': '5'
        }
        OrderItemStruct.load_from_dict(_dict)

    @raises(ValidateError)
    def test_orderitem_validate_wrong_id(self):
        _dict = {
            'product': '58',
            'quantity': '5'
        }
        OrderItemStruct.load_from_dict(_dict)

    @raises(ValidateError)
    def test_orderitem_validate_product_count(self):
        _dict = {
            'product': self.product.id,
            'quantity': '52'
        }
        order_item = OrderItemStruct.load_from_dict(_dict)
        order_item.validate()

    def test_orderitem(self):
        _dict = {
            'product': self.product.id,
            'quantity': '45'
        }
        order_item = OrderItemStruct.load_from_dict(_dict)
        order_item.validate()
        self.assertEqual(order_item.get_price(), Decimal('9000'))

    @raises(ValidateError)
    def test_order_validate_delivery(self):
        _dict = {
            'order_items': self.order_item_list,
            'delivery_type': '87',
            'user': self.user.id
        }
        OrderStruct.load_from_dict(_dict)

    @raises(ValidateError)
    def test_order_validate_user(self):
        _dict = {
            'order_items': self.order_item_list,
            'delivery_type': self.delivery.id,
            'user': ''
        }
        order = OrderStruct.load_from_dict(_dict)
        order.validate()

    @raises(ValidateError)
    def test_order_validate_payment(self):
        _dict = {
            'order_items': self.order_item_list,
            'delivery_type': self.delivery.id,
            'user': self.user.id,
            'payment_type': 'Mass'
        }
        order = OrderStruct.load_from_dict(_dict)
        order.validate()

    @raises(ValidateError)
    def test_order_validate_order_item(self):
        _dict = {
            'order_items': '',
            'delivery_type': self.delivery.id,
            'user': self.user.id
        }
        order = OrderStruct.load_from_dict(_dict)
        order.validate()

    def test_order(self):
        _dict = {
            'order_items': self.order_item_list,
            'delivery_type': self.delivery.id,
            'user': self.user.id
        }
        order = OrderStruct.load_from_dict(_dict)
        order.validate()
        self.assertEqual(order.get_product_list_id(), [self.product.id])
        self.assertEqual(order.get_price(), Decimal('3000'))
        self.assertEqual(order.get_quantity(), 15)
        product2 = create_product(
            name='Car',
            price=100,
            category = self.category
        )
        order_item2 = OrderItemStruct.load_from_dict({
            'product': product2.id,
            'quantity': '5'
        })
        order.add_order_item(order_item2)
        self.assertEqual(order.get_quantity(), 20)
        order_item3 = OrderItemStruct.load_from_dict({
            'product': product2.id,
            'quantity': '8'
        })
        order.add_order_item(order_item3)
        self.assertEqual(order.get_quantity(), 23)
        order.save_to_db()
        self.assertEqual(Order.objects.all().count(), 1)
        self.assertEqual(OrderItem.objects.all().count(), 2)
        order.del_item(self.product.id)
        self.assertEqual(len(order.get_product_list_id()), 1)

    def test_storage(self):
        _dict = {
            'order_items': self.order_item_list,
            'delivery_type': self.delivery.id,
        }
        order = OrderStruct.load_from_dict(_dict)
        request = FakeRequest()
        self.assertIsNone(RedisOrderStorage(request).get_from_storage())
        RedisOrderStorage(request).set_to_storage(order)
        order_result = RedisOrderStorage(request).get_from_storage()
        self.assertEqual(order.uid, order_result.uid)
        RedisOrderStorage(request).del_from_storage()
        self.assertIsNone(RedisOrderStorage(request).get_from_storage())

class ApiTestOrder(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user = mommy.make(User, first_name='Alex', last_name='Kerd', email='ex@example.com')
        self.delivery = mommy.make(Delivery, title='Nova Poshta')
        category = mommy.make(Category, name=u"Boys", uid=1)
        category2 = mommy.make(Category, name=u"Girls", uid=2)
        self.product1 = create_product(
            name=u'Автобус инерционый',
            price=150,
            category = category
        )
        self.product2 = create_product(
            name=u'Машинка',
            price=52,
            category = category
        )
        self.product3 = create_product(
            name=u'Кукла',
            price=75,
            category = category2
        )
        self.request = FakeRequest()
        self.request.session.session_key = None
        mommy.make(ExtraInfo, price_limit=1)
        RedisOrderStorage(self.request).del_from_storage()


    def test_add_change_order(self):
        error_dict = {
            'product': '58',
            'quantity': '5'
        }
        resp = self.app.post(reverse('api_order_add'), error_dict)
        self.assertTrue(resp.json.get('error'))
        prod1_dict = {
            'product': self.product1.id,
            'quantity': '5'
        }
        resp = self.app.post(reverse('api_order_add'), prod1_dict)
        self.assertEqual(
            resp.json['data']['order_items'],
            [{'product': self.product1.id, 'quantity': 5, 'price': 150.0}]
        )
        prod1_dict['quantity']='2'
        resp = self.app.post(reverse('api_order_add'), prod1_dict)
        self.assertEqual(resp.json['data']['price'], 300.0)
        prod2_dict = {
            'product': self.product2.id,
            'quantity': '4'
        }
        resp = self.app.post(reverse('api_order_add'), prod2_dict)
        self.assertEqual(len(resp.json['data']['order_items']), 2)
        prod3_dict = {
            'product': self.product3.id,
        }
        resp = self.app.post(reverse('api_order_add'), prod3_dict)
        self.assertEqual(len(resp.json['data']['order_items']), 3)
        resp = self.app.post(
            reverse('api_product_del'),
            {'product': unicode(self.product2.id)}
        )
        self.assertTrue(resp.json['success'])
        resp = self.app.post(
            reverse('api_product_del'),
            {'product': unicode(self.product1.id)}
        )
        self.assertTrue(resp.json['success'])
        resp = self.app.post(
            reverse('api_product_del'),
            {'product': unicode(self.product3.id)}
        )
        self.assertTrue(resp.json['success'])
        self.assertIsNone(RedisOrderStorage(self.request).get_from_storage())
        prod1_dict = {
            'product': self.product1.id,
            'quantity': '58'
        }
        resp = self.app.post(reverse('api_order_add'), prod1_dict)
        self.assertIsNotNone(RedisOrderStorage(self.request).get_from_storage())
        resp = self.app.post(reverse('api_order_remove'))
        self.assertIsNone(RedisOrderStorage(self.request).get_from_storage())

    def test_approve_order(self):
        prod1_dict = {
            'product': self.product1.id,
            'quantity': '48'
        }
        resp = self.app.post(reverse('api_order_add'), prod1_dict)
        _uid = resp.json['data']['uid']
        resp = self.app.get(
            reverse('api_order_approve'),
            {'delivery': ''},
        )
        self.assertFalse(resp.json['success'])
        resp = self.app.get(
            reverse('api_order_approve'),
            {'delivery': self.delivery.id},
            user=self.user
        )
        self.assertTrue(resp.json['success'])
        self.assertIsNone(RedisOrderStorage(self.request).get_from_storage())
        self.assertTrue(Order.objects.filter(uid=_uid))
        #TODO test send email
