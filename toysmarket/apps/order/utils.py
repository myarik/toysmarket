# -*- coding: utf-8 -*-
from toysmarket.apps.extra_settings.models import ExtraInfo
from celery import current_app


def post_order(order):
    admin_email = ExtraInfo.objects.get(pk=1).email_admin
    dict_value = {
        'order': order,
        'order_goods': order.order_items,
    }
    if admin_email:
        current_app.send_task(
            'emails.send_email',
            args=[
                'order_admin',
                admin_email,
                dict_value,
                u'777toys.com.ua: Новый заказа на сайте %s' % order.uid,
            ],
        )
    current_app.send_task(
        'emails.send_email',
        args=[
            'user_order',
            order.user.email,
            dict_value,
            u'777toys.com.ua: Ваш заказа-%s' % order.uid
        ],
    )
