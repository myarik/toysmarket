# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.conf import settings
from django.db.models.deletion import PROTECT

from django.db import models

from decimal import Decimal

from django.db.models.signals import post_save
from django.dispatch import receiver
from toysmarket.apps.product.models import Product


class Delivery(models.Model):
    """ Delivery """
    title = models.CharField(max_length=255, verbose_name=_('Заголовок'))

    class Meta:
        verbose_name = _("Способ доставки")
        verbose_name_plural = _("Способы доставки")

    def __unicode__(self):
        return unicode(self.title)


class Order(models.Model):
    """Model for user`s orders."""
    PAYED = 0
    PROCESS = 1

    CACHE = 0
    WITHOUT_CACHE = 1

    ORDER_STATUS = (
        (PAYED, _("Оплачен")),
        (PROCESS, _("В процессе"))
    )

    PAYMENT_TYPE = (
        (CACHE, _('Наличными')),
        (WITHOUT_CACHE, _('Безналичными'))
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_('Покупатель'),
                             related_name='userorder',
                             on_delete=PROTECT)
    uid = models.CharField(max_length=50, unique=True)
    delivery_type = models.ForeignKey(Delivery,
                                      verbose_name=_('Способ доставки'))
    payment_type = models.PositiveSmallIntegerField(_('Способ оплаты'),
                                                    choices=PAYMENT_TYPE,
                                                    default=CACHE)
    created_date = models.DateTimeField(auto_now_add=True)
    status = models.PositiveSmallIntegerField(_('Статус'),
                                              choices=ORDER_STATUS,
                                              default=PROCESS)
    delivery_address = models.CharField(
        max_length=250, blank=True, null=True, verbose_name=u'Адресс доставки')
    comment = models.TextField(verbose_name=_('Комментарий к заказу'),
                               blank=True,
                               null=True)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    def __unicode__(self):
        return u"Order:%s" % (self.uid)

    def get_order_price(self):
        return sum([item.get_item_price() for item in self.orderitem.all()])
    get_order_price.short_description = _('Цена заказа')

    def get_user_information(self):
        return '<a href="/admin/accounts/user/%s">%s %s</a>' % (
            self.user.id,
            self.user.first_name,
            self.user.last_name)
    get_user_information.short_description = _('Имя, Фамилия')
    get_user_information.allow_tags = True

    def get_user_phone(self):
        return "%s" % self.user.phone
    get_user_phone.short_description = _('Телефон')


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='orderitem',
                              verbose_name=_('Заказ'))
    product = models.ForeignKey(Product, verbose_name=_('Товар'))
    quantity = models.SmallIntegerField(_('Количество'))

    class Meta:
        verbose_name = _('OrderItem')
        verbose_name_plural = _('OrderItems')

    def __unicode__(self):
        return u'Заказа #%s Название товара: %s' % (self.order.uid, self.product.name)

    def get_item_price(self):
        return Decimal(self.product.price *
                       self.quantity).quantize(Decimal('.01'))

    @property
    def name(self):
        return self.product.name

    @property
    def sku(self):
        return self.product.sku

    @property
    def price(self):
        return self.product.price


@receiver(post_save, sender=Order, dispatch_uid="order_unique")
def approve_purchase(sender, instance, **kwargs):
    """
        Order changed status
    """
    if instance.status == Order.PAYED:
        for item in instance.orderitem.all():
            if not item.product.total_sold:
                item.product.total_sold = item.quantity
            else:
                item.product.total_sold += item.quantity
            if not item.product.total_sold < item.product.items_in_stock:
                item.product.allow_buy = False
            item.product.save()
            item.product.category.clean_cache()
