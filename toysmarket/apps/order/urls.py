# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from toysmarket.apps.order.views import BasketView


urlpatterns = patterns('order.api',
    url(r'^add/$', 'order_add', name='api_order_add'),
    url(r'^product_del/$', 'product_del', name='api_product_del'),
    url(r'^clean/$', 'order_remove', name='api_order_remove'),
    url(r'^approve/$', 'order_approve', name='api_order_approve'),
)

urlpatterns += patterns(
    '',
    url(r'^basket/$', BasketView.as_view(), name='basket'),
)
