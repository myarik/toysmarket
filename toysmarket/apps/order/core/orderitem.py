# -*- coding: utf-8 -*-
from .utils import get_product

from decimal import Decimal

from toysmarket.apps.core.base_struct import BaseStruct
from toysmarket.apps.core.error_exeption import ValidateError


class OrderItemStruct(BaseStruct):
    """OrderItem"""
    product = None
    quantity = 1

    def validate(self):
        if not self.product:
            raise ValidateError(message='Product did not choices')
        if self.product.items_in_stock:
            if not self.product.total_sold:
                self.product.total_sold = 0
            total_quantity = self.product.total_sold + self.quantity
            if total_quantity > self.product.items_in_stock:
                allow_quantity = \
                    self.quantity - (total_quantity-self.product.items_in_stock)
                raise ValidateError(
                    message=u'В наличии нет такого количества \
данного товара, Вы можете заказать %s' % int(allow_quantity))
        else:
            raise ValidateError(
                message=u'Извините товара %s уже \
                нет в наличии' % self.product.name)

    @classmethod
    def load_from_dict(cls, idict):
        """
            Creates struct from dict
        """
        obj = super(OrderItemStruct, cls).load_from_dict(idict)
        obj.product = get_product(idict.get('product'))
        obj.quantity = int(obj.quantity or 1)
        return obj

    def get_info(self):
        info = super(OrderItemStruct, self).get_info()
        info.update({
            'product': self.product.pk if self.product else None,
            'price': float(self.product.price)
        })
        return info

    def get_price(self):
        return Decimal(self.product.price * self.quantity)
