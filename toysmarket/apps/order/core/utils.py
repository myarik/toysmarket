# -*- coding: utf-8 -*-
from toysmarket.apps.core.error_exeption import ValidateError
from toysmarket.apps.order.models import Delivery
from toysmarket.apps.product.models import Product

import random
from string import digits, ascii_uppercase

legals = digits + ascii_uppercase
uuid_length = 10


def get_product(product_id):
    """
        Return product given id
    """
    try:
        product_id = int(product_id)
    except:
        raise ValidateError(message=u'Некоректно укзана ID продукта')
    try:
        return Product.objects.get(pk=product_id)
    except Product.DoesNotExist:
        raise ValidateError(message=u'Продукт не найден')


def get_delivery(delivery_id):
    """
        Return delivery given id
    """
    try:
        delivery_id = int(delivery_id)
    except:
        raise ValidateError(message=u'Некоректно укзана ID доставки')
    try:
        return Delivery.objects.get(pk=delivery_id)
    except Delivery.DoesNotExist:
        raise ValidateError(message=u'Способ доставки не найден')


def rand_string(length=uuid_length, char_set=legals):
    return ''.join(random.choice(char_set) for _ in range(length))
