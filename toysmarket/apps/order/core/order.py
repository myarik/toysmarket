# -*- coding: utf-8 -*-
import datetime
import time

from toysmarket.apps.core.base_struct import BaseStruct
from toysmarket.apps.core.error_exeption import ValidateError
from toysmarket.apps.order.core.orderitem import OrderItemStruct

from toysmarket.apps.order.models import Order, OrderItem
from toysmarket.apps.extra_settings.models import ExtraInfo

from .utils import get_delivery, rand_string

from django.utils.functional import SimpleLazyObject
from django.contrib.auth import get_user_model

from django.core.exceptions import ObjectDoesNotExist


class OrderStruct(BaseStruct):
    user = None
    uid = None
    order_items = None
    delivery_type = None
    create_time = None
    payment_type = Order.CACHE
    comment = None

    def __init__(self, **kwargs):
        super(OrderStruct, self).__init__(**kwargs)
        self.uid = kwargs.get('uid', rand_string())
        self.create_time = datetime.datetime.now()

    def validate(self):
        if not self.order_items:
            raise ValidateError(message=u'Товар не выбран')

        if not self.delivery_type:
            raise ValidateError(message=u'Не указан способ доставки')

        if not self.user:
            raise ValidateError(
                message=u'Для совершения покупок нужно авторизироватся')

        if (self.payment_type is None):
            raise ValidateError(message=u'Не указан способ доставки')

        if self.payment_type not in dict(Order.PAYMENT_TYPE).keys():
            raise ValidateError(message=u"Способ оплаты указан не корректно")

        min_price = ExtraInfo.objects.get(pk=1).price_limit
        if self.get_price() < min_price:
            raise ValidateError(
                message=u"Минимальная сумма заказа %s грн." % min_price)

        for order_item in self.order_items:
            order_item.validate()

    @classmethod
    def load_from_dict(cls, idict):
        """Creates order from dict"""
        obj = super(OrderStruct, cls).load_from_dict(idict)
        obj.order_items = [
            OrderItemStruct.load_from_dict(x) for x in obj.order_items
            if obj.order_items
        ]
        if idict.get('user'):
            obj.user = SimpleLazyObject(lambda: (
                get_user_model().objects.get(pk=idict.get('user')))
            )
        if idict.get('delivery_type'):
            obj.delivery_type = get_delivery(idict.get('delivery_type'))
        if isinstance(idict.get('create_time'), basestring):
            format_string = "%Y-%m-%d"
            obj.create_time = datetime.datetime.strptime(idict['create_time'], format_string).date()
        elif not isinstance(obj.create_time, datetime.date):
            obj.create_time = datetime.datetime(obj.create_time) if obj.create_time else None
        if isinstance(idict.get('payment_type'), basestring):
            try:
                obj.payment_type = int(idict.get('payment_type'))
            except:
                pass
        return obj

    @classmethod
    def load_from_db(cls, uid):
        """
            Load booking from db and return new BookingStruct instance
        """
        obj = Order.objects.get(uid=uid)
        return obj

    def get_info(self):
        """Get Info"""
        info = super(OrderStruct, self).get_info()
        fmt = '%Y-%m-%d'
        order_items = map(lambda x: x.get_info(), self.order_items)
        info.update({
            'user': self.user.pk if self.user else None,
            'delivery_type': self.delivery_type.pk if self.delivery_type else None,
            'order_items': order_items,
            'create_time': self.create_time.strftime(fmt),
            'price': float(self.get_price())
        })
        return info

    def update_delivery(self, delivery_id):
        self.delivery_type = get_delivery(delivery_id)

    def get_product_list_id(self):
        return map(lambda x: x.product.pk, self.order_items)

    def get_price(self):
        return sum([item.get_price() for item in self.order_items])

    def get_quantity(self):
        return sum([item.quantity for item in self.order_items])

    def get_product_list(self):
        return map(lambda x: x.product, self.order_items)

    def add_order_item(self, order_item):
        """
        Product add or update in order
        @param order_item: Add order
        """
        order_item.validate()
        new_item = True
        for item in self.order_items:
            if item.product.pk == order_item.product.pk:
                item.quantity = order_item.quantity
                new_item = False
        if new_item:
            self.order_items.append(order_item)

    def del_item(self, product_id):
        """
        Remove product from order
        @param product_id: Product id
        """
        try:
            product_id = int(product_id)
        except:
            raise ValidateError(message=u'Не правильно указан id товара')
        for item in self.order_items:
            if item.product.pk == product_id:
                self.order_items.remove(item)

    def save_to_db(self):
        """Save to DATA BASE"""
        new_info = False
        try:
            record = Order.objects.get(uid=self.uid)
        except ObjectDoesNotExist:
            new_info = True
            record = Order(
                uid=self.uid,
                user=self.user,
                delivery_type=self.delivery_type,
                payment_type=self.payment_type,
                delivery_address=self.user.adress.all()[0].city if self.user.adress.all() else None,
                comment=self.comment
            )
            record.save()
        if new_info:
            for order_item in self.order_items:
                OrderItem.objects.create(
                    order=record,
                    product=order_item.product,
                    quantity=order_item.quantity
                )
