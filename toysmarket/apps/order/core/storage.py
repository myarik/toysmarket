# -*- coding: utf-8 -*-
import redis
import json
from django.conf import settings
from toysmarket.apps.order.core.order import OrderStruct


class RedisOrderStorage(object):
    key_mask = 'ORDER_%s'

    def __init__(self, request):
        self.key = self.key_mask % request.session.session_key
        host, port, db, password = settings.REDIS_STORAGE
        self.redis_instance = redis.StrictRedis(
            host=host,
            port=port,
            db=db,
            password=password
        )

    def decimal_default(self, obj):
        """
            handle decimals for json by converting to string
        """
        if isinstance(obj, decimal.Decimal):
            return str(obj)
        raise TypeError

    def get_from_storage(self):
        """
            Get order info from redis as json string.
            Deserialize and build OrderStruct
        """
        order = self.redis_instance.get(self.key)
        if order:
            order = json.loads(order)
            return OrderStruct.load_from_dict(order)

    def set_to_storage(self, order):
        """
            Serialize order to JSON string and
            store at redis
        """
        order = json.dumps(order.get_info(), default=self.decimal_default)
        self.redis_instance.setex(self.key, settings.BASKET_SESSION_TIMEOUT, order)

    def del_from_storage(self):
        """
            Remove order from redis
        """
        self.redis_instance.delete(self.key)