# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.views.generic import TemplateView

from toysmarket.apps.order.core.storage import RedisOrderStorage
from toysmarket.apps.order.models import Delivery, Order
# Create your views here.


class BasketView(TemplateView):
    """
    Basket Page
        template: basket.html
    """
    template_name = 'basket.html'

    def get_context_data(self, **kwargs):
        context = super(BasketView, self).get_context_data(**kwargs)
        order = RedisOrderStorage(self.request).get_from_storage()
        if order:
            context['product_list'] = []
            for item in order.order_items:
                product = item.product
                product.count = item.quantity
                product.sum = item.quantity * product.price
                context['product_list'].append(product)
        context['delivery_list'] = Delivery.objects.all()
        context['payment_list'] = Order.PAYMENT_TYPE
        return context
