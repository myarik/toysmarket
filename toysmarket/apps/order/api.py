# -*- coding: utf-8 -*-
import logging
import traceback

# from django.contrib.auth.decorators import login_required
# from django.shortcuts import redirect
# from django.contrib.sites.models import get_current_site
# from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

from toysmarket.apps.order.core.order import OrderStruct
from toysmarket.apps.order.core.orderitem import OrderItemStruct
from toysmarket.apps.order.core.storage import RedisOrderStorage

from toysmarket.apps.core.error_exeption import ValidateError

from toysmarket.apps.core.utils import post_required, ajax_request
from toysmarket.apps.order.utils import post_order

logger = logging.getLogger(__name__)


@csrf_exempt
@post_required
@ajax_request()
def order_add(request):
    '''
    Create order request
    '''
    try:
        item = OrderItemStruct.load_from_dict(request.POST)
        order = RedisOrderStorage(request).get_from_storage()
        if not order:
            order = OrderStruct(
                user=request.user if request.user.is_authenticated() else None,
                order_items=[item]
            )
        else:
            order.add_order_item(item)
        RedisOrderStorage(request).set_to_storage(order)
        return {
            'success': True,
            'data': order.get_info(),
            'count': order.get_quantity()
        }
    except ValidateError as err:
        return {
            'success': False,
            'error': {
                'message': err.message
            }
        }


@csrf_exempt
@post_required
@ajax_request()
def product_del(request):
    '''
        Product delete from order
    '''
    try:
        product_id = request.REQUEST.get('product')
        order = RedisOrderStorage(request).get_from_storage()
        if not order:
            return {
                'success': False,
                'error': {
                    'message': u'Заказ не найден'
                }
            }
        order.del_item(product_id)
        order_data = False
        if not order.get_product_list_id():
            RedisOrderStorage(request).del_from_storage()
        else:
            RedisOrderStorage(request).set_to_storage(order)
            order_data = order.get_info()
        return {
            'success': True,
            'message': u'Продукт удален с заказа',
            'data': order_data,
            'count': order.get_quantity()
        }
    except ValidateError:
        return {
                'success': False,
                'error': {
                    'message': u'Некорректно указан id'
                }
        }


@ajax_request()
def order_remove(request):
    '''
    Order delete
    '''
    RedisOrderStorage(request).del_from_storage()
    return {
            'success': True,
            'message': u'Заказ удален'
        }


@ajax_request()
def order_approve(request):
    '''Order approve, save data to database'''
    if request.user.is_authenticated():
        user = request.user
    else:
        return {
            'success': False,
            'error': {
                'message': u'Для совершения покупок нужно авторизироватся'
            }
        }
    order = RedisOrderStorage(request).get_from_storage()
    if not order:
        return {
            'success': False,
            'error': {
                'message': u'Заказ не найден'
            }
        }
    try:
        if request.REQUEST.get('delivery'):
            order.update_delivery(request.REQUEST.get('delivery'))
        if not order.user:
            order.user = user
        if request.REQUEST.get('payment_type'):
            if isinstance(request.REQUEST.get('payment_type'), basestring):
                try:
                    order.payment_type = int(
                        request.REQUEST.get('payment_type'))
                except:
                    order.payment_type = request.REQUEST.get('payment_type')
        order.comment = request.REQUEST.get('comment', order.comment)
        order.validate()
    except ValidateError as err:
        return {
            'success': False,
            'error': {
                'message': err.message,
            }
        }
    try:
        order.save_to_db()
        RedisOrderStorage(request).del_from_storage()
        post_order(order)
    except Exception as err:
        logger.exception(
            'Data was not written to database',
            {
                'error': unicode(err),
                'traceback': traceback.format_exc()
            }
        )
        return {
            'success': False,
            'error': {
                'message': u'Возникла ошибка на сайте, свяжытесь с администратором сайта. Спасибо',
            }
        }
    #TODO send email
    return {'success': True, 'message': u'Заказ создан'}
