# -*- coding: utf-8 -*-
from toysmarket.apps.order.core.storage import RedisOrderStorage


def basket_info(request):
    responce_dict = {}
    order = RedisOrderStorage(request).get_from_storage()
    if order:
        responce_dict['basket_price'] = order.get_price()
        responce_dict['basket_count'] = order.get_quantity()
    else:
        responce_dict['basket_price'], responce_dict['basket_count'] = 0, 0
    return responce_dict
