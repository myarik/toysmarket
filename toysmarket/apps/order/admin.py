# -*- coding: utf-8 -*-

from django.contrib import admin

# Register your models here.
from toysmarket.apps.order.models import Order, OrderItem, Delivery


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    fieldsets = [
        (None, {'fields': ['name', 'sku', 'price', 'quantity']}),
    ]
    readonly_fields = ('name', 'sku', 'price')


class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_display = (
        'uid',
        'get_user_information',
        'get_user_phone',
        'payment_type',
        'delivery_type',
        'delivery_address',
        'created_date',
        'status',
        'get_order_price',
    )
    inlines = (OrderItemInline,)

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(Delivery)
