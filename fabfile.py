#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from fabric.api import *

import os

env.roledefs['production'] = ['toysmarket@toys.myarik.com:81']


def start_project():
    """
        Some things, when you start project
    """
    local('pip install -r requirements.txt')
    import random
    prompt('Insert in local.py SECRET_KEY: '+"".join([
        random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)")
        for i in range(50)
    ]))
    prompt(
        'Fix .gitignore change dj_skeleton for YOU_PROJECT_NAME\
and rename dir dj_skeleton'
    )


def production_env():
    env.project_root = '/home/toysmarket/toysmarket'  # Путь до каталога проекта (на сервере)
    env.activate = 'source ' + os.path.join(env.project_root, '.env/bin/activate')
    env.branch = 'master'
    env.python = '/home/toysmarket/toysmarket/.env/bin/python'
    env.pip = '/home/toysmarket/toysmarket/.env/bin/pip'


def virtualenv(command):
    with cd(env.directory):
        run(env.activate + '&&' + command)


@roles('production')
def deploy():
    production_env()
    with cd(env.project_root):
        run('git checkout %s' % env.branch)
        run('git pull origin master')  # Пуляемся из репозитория
        run('find . -name "*.pyc" -print -delete')  # Чистим старые скомпиленные файлы gettext'а
        run('{pip} install --upgrade -r {filepath}'.format(pip=env.pip,
            filepath=os.path.join(env.project_root, 'requirements.txt')))
        run('{} manage.py collectstatic --noinput'.format(env.python))  # Собираем статику
        run('touch /tmp/toysmarket.txt')


@roles('production')
def status():
    run('sudo /usr/local/bin/supervisorctl status')


@roles('production')
def restart():
    run('sudo /usr/local/bin/supervisorctl restart toysmarket')
    run('sudo /usr/local/bin/supervisorctl restart toysmarket_celery')
    run('sudo /usr/local/bin/supervisorctl restart sphinx')


def go():
    comment = raw_input('Enter Comment: ')
    local('git add .')
    local('git commit -m "%s"' % comment)
    local('git push origin master')
